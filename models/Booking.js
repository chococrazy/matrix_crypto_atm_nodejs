var bookshelf = require('../config/bookshelf');
var Atm = require('../models/Atm');
var User = require('../models/User');

var Booking = bookshelf.Model.extend(
{
  tableName: 'bookings',
  hasTimestamps: true,
  atm: function()
  {
    return this.belongsTo(Atm, 'listing_id');
  },
  user: function()
  {
    return this.belongsTo(User);
  }

});

module.exports = Booking;
