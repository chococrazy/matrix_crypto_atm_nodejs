var bookshelf2 = require('../../config/bookshelf2');


var Bills               = bookshelf2.Model.extend({tableName: 'bills'});
var CashInActions       = bookshelf2.Model.extend({tableName: 'cash_in_actions'});
var CashInRefills       = bookshelf2.Model.extend({tableName: 'cash_in_refills'});
var CashInTxs           = bookshelf2.Model.extend({tableName: 'cash_in_txs'});
var CashOutActions      = bookshelf2.Model.extend({tableName: 'cash_out_actions'});
var CashOutRefills      = bookshelf2.Model.extend({tableName: 'cash_out_refills'});
var CashOutTxs          = bookshelf2.Model.extend({tableName: 'cash_out_txs'});
var ComplianceOverrides = bookshelf2.Model.extend({tableName: 'compliance_overrides'});
var Customers           = bookshelf2.Model.extend({tableName: 'customers'});
var Devices             = bookshelf2.Model.extend({tableName: 'devices'});
var Trades              = bookshelf2.Model.extend({tableName: 'trades'});

module.exports = {
  Bills               : Bills,
  CashInActions       : CashInActions,
  CashInRefills       : CashInRefills,
  CashInTxs           : CashInTxs,
  CashOutActions      : CashOutActions,
  CashOutRefills      : CashOutRefills,
  CashOutTxs          : CashOutTxs,
  ComplianceOverrides : ComplianceOverrides,
  Customers           : Customers,
  Devices             : Devices,
  Trades              : Trades,
}
