
var bookshelf = require('../config/bookshelf');
var Category = require('./Category');

var BlogPost = bookshelf.Model.extend({
  tableName: 'blogpost',
  hasTimestamps: true,
	 category: function() {
    return this.belongsTo(Category);
  }
});

module.exports = BlogPost;
