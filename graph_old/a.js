

function toTimeZone(time, zone) {
  if(!zone)
    return moment(time);
  return moment(time).tz(zone);
}



class Clock {

  /**
   * Clock initialization
   */
  constructor(id, zone) {
    this.id = id;
    this.zone = zone;
    this.hourHand   = document.querySelector('.clock_'+id + ' .hour.hand');
    this.minuteHand = document.querySelector('.clock_'+id + ' .minute.hand');
    this.secondHand = document.querySelector('.clock_'+id + ' .second.hand');
    this.timer();
    setInterval(() => this.timer(), 1000);
  }

  /**
   * Timer of the clock
   */
  timer() {



    this.sethandRotation('hour');
    this.sethandRotation('minute');
    this.sethandRotation('second');
  }

  /**
   * Changes the rotation of the hands of the clock
   * @param  {HTMLElement} hand   One of the hand of the clock
   * @param  {number}      degree degree of rotation of the hand
   */
  sethandRotation(hand) {
    let date = new Date(), hours, minutes, seconds, percentage, degree;
    // if(this.zone){
      date = toTimeZone(date, this.zone);
    // }
    switch (hand) {
      case 'hour':
        hours       = date.hour();
        hand        = this.hourHand;
        percentage  = this.numberToPercentage(hours, 12);
        break;
      case 'minute':
        minutes     = date.minutes();
        hand        = this.minuteHand;
        percentage  = this.numberToPercentage(minutes, 60);
        break;
      case 'second':
        seconds     = date.seconds();
        hand        = this.secondHand;
        percentage  = this.numberToPercentage(seconds, 60);
        // this.sound.play();
        break;
    }

    degree                = this.percentageToDegree(percentage);
    hand.style.transform  = `rotate(${degree}deg) translate(-50%, -50%)`;
  }

  /**
   * Converting a number to a percentage
   * @param  {number} number Number
   * @param  {number} max    Maximum value of the number
   * @return {number}        Return a percentage
   */
  numberToPercentage(number = 0, max = 60) {
    return (number / max) * 100;
  }

  /**
   * Converting a percentage to a degree
   * @param  {number} percentage Percentage
   * @return {number}            Return a degree
   */
  percentageToDegree(percentage = 0) {
    return (percentage * 360) / 100;
  }

}

let clock1 = new Clock('1', 'Asia/Tokyo');
let clock2 = new Clock('2', 'Asia/Tel_Aviv');
let clock3 = new Clock('3', 'America/New_York');
