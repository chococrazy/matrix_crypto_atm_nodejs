var app =   angular.module('MyApp', []);
// .run(function($rootScope, $window, $http) {
//
// })



app
  .controller('CryptCtrl', function($scope, $http)
  {
    $scope.tableData = [];
    $scope.tm  = moment().format('DD MMM, YYYY')

    $scope.toDt = function(a)
    {
      return moment.unix(a).format('HH:mm DD/MM/YY')
    }

    $scope.fetchPRice = function( )
    {
      $scope.tm  = moment().format('DD MMM, YYYY')
      $http.get('/fetchGraph' )
        .then(function(response)
        {
          console.log(response)
          if(response.data.ok)
          {
            $scope.tableData = [];
            var data = response.data.prices.data;
            // console.log(43)
            // console.log(response.data.prices)
            $scope.tableData =  data;


          }


          // $scope.blogposts = response.data.posts;
        })
        .catch(function(response)
        {
          console.log(response);
          $scope.tableData = [];

        });
    }
    $scope.fetchPRice( );
    setInterval(function( ){
      $scope.fetchPRice( );
    }, 30000);
  });
