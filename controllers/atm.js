var Atm = require('../models/Atm');
var moment = require('moment');


exports.deleteAtm = function(req, res, next) {
  new Atm({ id: req.body.id }).destroy().then(function(user) {
    res.send({ msg: 'The ATM has been successfully deleted.' });
  }).catch(function(err) {
    return res.status(400).send({ msg : 'Something went wrong while deleting the ATM listing' });
  });
};

exports.updateAtm = function(req, res, next) {
  req.assert('title'   , 'Title cannot be blank').notEmpty();
  req.assert('description' , 'Description cannot be blank').notEmpty();

  var errors = req.validationErrors();
  if (errors) {
    return res.status(400).send(errors);
  }

  var atm = new Atm({ id: req.body.id });

  var close = moment(req.body.close_time, "HH:mm:ss").format("HHmm");
  var open = moment(req.body.start_time, "HH:mm:ss").format("HHmm");
  if(open >= close)
  {
    return res.status(400).send({ msg : 'Close time should be bigger than start time' });
  }

  atm.save({
    title           : req.body.title,
    title_he        : req.body.title_he ?req.body.title_he : req.body.title ,
    description_he  : req.body.description_he ?req.body.description_he : req.body.description ,
    address_he      : req.body.address_he ?req.body.address_he : req.body.address ,
    description     : req.body.description,
    address         : req.body.address,
    phone_number    : req.body.phone_number,
    commission      : req.body.commission,
    commission_sell : req.body.commission_sell,
    lat             : req.body.lat,
    long            : req.body.long,
    days_available  : req.body.days_available,
    close_time      : moment(req.body.close_time, "HH:mm:ss").format("HH:mm"),
    start_time      : moment(req.body.start_time, "HH:mm:ss").format("HH:mm"),
    has_buy_option  : (req.body.has_buy_option == 'yes' ) ? true : false,
    has_sell_option : (req.body.has_sell_option == 'yes') ? true : false,

  }).then(function(atm) {
    atm.fetch().then(function(){
      res.send({ atm : atm, msg: 'ATM listing has been updated.' });
    }).catch(function(err) {
      // console.log(err);
      res.status(400).send({ msg : 'Something went wrong while updating the ATM listing' });
    });

  }).catch(function(err) {
    console.log(err);
    res.status(400).send({ msg : 'Something went wrong while updating the ATM listing' });
  });
};

exports.addAtm = function(req, res, next) {
  req.assert('title'   , 'Title cannot be blank').notEmpty();
  req.assert('description' , 'Description cannot be blank').notEmpty();

  var errors = req.validationErrors();

  if (errors) {
    return res.status(400).send(errors);
  }

  var close = moment(req.body.close_time, "HH:mm:ss").format("HHmm");
  var open = moment(req.body.start_time, "HH:mm:ss").format("HHmm");
  if(open >= close)
  {
    return res.status(400).send({ msg : 'Close time should be bigger than start time' });
  }


  new Atm({
    title           : req.body.title,
    description     : req.body.description,
    address         : req.body.address,
    title_he        : req.body.title_he ?req.body.title_he : req.body.title ,
    description_he  : req.body.description_he ?req.body.description_he : req.body.description ,
    address_he      : req.body.address_he ?req.body.address_he : req.body.address ,
    phone_number    : req.body.phone_number,
    lat             : req.body.lat,
    long            : req.body.long,
    commission      : req.body.commission,
    commission_sell : req.body.commission_sell,
    days_available  : req.body.days_available,
    close_time      : moment(req.body.close_time, "HH:mm:ss").format("HH:mm"),
    start_time      : moment(req.body.start_time, "HH:mm:ss").format("HH:mm"),
    has_buy_option  : (req.body.has_buy_option == 'yes' ) ? true : false,
    has_sell_option : (req.body.has_sell_option == 'yes') ? true : false,

  }).save()
  .then(function(atm) {
      res.send({ ok:true , msg: 'New ATM listing has been created successfully.' });
  })
  .catch(function(err) {
    console.log(err)
       return res.status(400).send({ msg: 'Something went wrong while created a new ATM listing' });
   });
};

exports.listAtmSearch = function(req, res, next){
  // console.log(req.body)
  var filter = (req.body) ? req.body : ( {text:false , location:false, service: false });

  // console.log(filter);

  var atms =   new Atm( )

  // console.log(filter.location);
  if(filter.location)
  {
    atms = atms.query(function (qb) {
      qb.whereRaw(`LOWER(address) LIKE ?`, [`%${filter.location.toLowerCase()}%`])
    })
  }
  if(filter.service)
  {
    if(filter.service !='both')
    {
      if(filter.service == 'sell')
      {
        atms = atms.where('has_sell_option', '=', true);
      }else if(filter.service == 'buy')
      {
        atms = atms.where('has_buy_option', '=', true);
      }
    }

  }
  if(filter.text)
  {
    atms = atms.query(function (qb) {
      qb.whereRaw(`LOWER(title) LIKE ?`, [`%${filter.text.toLowerCase()}%`])
    })


  }

  atms.fetchAll().then(function(atms) {
    if (!atms) {
      return res.status(200).send({atms:[], user: req.user});
    }
    return res.status(200).send({ok:true , atms: atms.toJSON(), user: req.user});
  })
  .catch(function(err) {
    console.log(err)
    return res.status(200).send({ atms:[],user: req.user});
  });

}
exports.listAtm = function(req, res, next) {


  new Atm( )
  .orderBy('id', 'DESC')
  .fetchAll()
  .then(function(atms) {
    if (!atms) {
      return res.status(200).send({atms :[], user: req.user});
    }
    return res.status(200).send({ok:true , atms: atms.toJSON()});
  })
  .catch(function(err) {
    return res.status(200).send({atms :[], user: req.user});
  });
};


exports.listSingleAtm = function(req, res, next)
{
  new Atm( ).where('id', req.params.id)
   .fetch()
   .then(function(atm) {
     if (!atm) {
       return res.status(200).send({id : req.params.id, title: '',description: ''});
     }
     return res.status(200).send({ok:true , atm: atm.toJSON()});
   })
   .catch(function(err) {
     return res.status(400).send({id : req.params.id, title: '',description: '', msg: 'failed to fetch from db'});
   });

}
