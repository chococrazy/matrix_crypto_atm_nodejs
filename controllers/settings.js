var fs = require('fs');
var Settings = require('../models/Settings');


exports.deleteSettings = function(req, res, next) {
  new Settings({ id: req.body.id }).destroy().then(function(user) {
    res.send({ msg: 'The settings has been successfully deleted.' });
  }).catch(function(err) {
    return res.status(400).send({ msg : 'Something went wrong while deleting the QA item' });
  });
};


exports.create_SATOSHIS = function(req, res, next)
{


  var s = [

    //     {
    //       key      : 'home_page_photo',
    //       label    : 'Home Page Image',
    //       type     : 'file',
    //       autoload : true,
    //       page     : 'Home'
    //     },
    //
    //
    //     {
    //       key      : 'about_page_photo',
    //       label    : 'About Page Image',
    //       type     : 'file',
    //       autoload : true,
    //       page     : 'About'
    //     },
		// 		  {
    //   key      : 'social_facebook',
    //   label    : 'Facebook - URL',
    //   type     : 'url',
    //   content  : 'http://facebook.com',
    //   autoload : true,
    //   page     : 'Social'
    // },
    // {
    //   key      : 'social_twitter',
    //   type     : 'url',
    //   label    : 'Twitter - URL',
    //   content  : 'http://twitter.com',
    //   autoload : true,
    //   page     : 'Social'
    // },
    // {
    //   key      : 'social_telegram',
    //   type     : 'url',
    //   label    : 'Telegram - URL',
    //   content  : 'http://telegram.com',
    //   autoload : true,
    //   page     : 'Social'
    // },
    // {
    //   key      : 'contact_number',
    //   type     : 'text',
    //   label    : 'Contact Phone Number',
    //   content  : ' 03-111-23456',
    //   autoload : true,
    //   page     : 'Contact'
    // },
    // {
    //   key      : 'contact_email',
    //   type     : 'email',
    //   label    : 'Contact Email',
    //   content  : 'info@yourcompany.com',
    //   autoload : true,
    //   page     : 'Contact'
    // },
    // {
    //   key      : 'contact_address',
    //   type     : 'text',
    //   label    : 'Contact Address',
    //   content  : '96 Woodside USA Address',
    //   autoload : true,
    //   page     : 'Contact'
    // },
    // {
    //   key      : 'contact_timings',
    //   type     : 'text',
    //   label    : 'Contact Timings',
    //   content  : 'Sunday through Thursday: 10:00 - 20:00',
    //   autoload : true,
    //   page     : 'Contact'
    // },
    // {
    //   key      : 'footer_text',
    //   type     : 'text',
    //   label    : 'Footer Text',
    //   content  : '&copy; 2017 Matrix Inc. Purchase Agreement Service Level Agreement Due Diligence',
    //   autoload : true,
    //   page     : 'Footer'
    // },
    // {
    //   key      : 'about_short',
    //   type     : 'textarea',
    //   label    : 'About Short',
    //   content  : 'Our rates are the best in Israel. We support the deposit of Fiat, the purchase of currencies, the withdrawal of Fiat and the sale of currencies by deploying more than 50 ATMs throughout Gush Dan Area.',
    //   autoload : true,
    //   page     : 'Footer'
    // },
    // {
    //   key      : 'about_large',
    //   type     : 'wysiwyg',
    //   label    : 'About Large',
    //   content  : 'Our rates are the best in Israel. We support the deposit of Fiat, the purchase of currencies, the withdrawal of Fiat and the sale of currencies by deploying more than 50 ATMs throughout Gush Dan Area.',
    //   autoload : true,
    //   page     : 'About'
    // },
    // {
    //   key      : 'for_traders',
    //   type     : 'wysiwyg',
    //   label    : 'For Traders - text',
    //   content  : 'Our rates are the best in Israel. We support the deposit of Fiat, the purchase of currencies, the withdrawal of Fiat and the sale of currencies by deploying more than 50 ATMs throughout Gush Dan Area. There is also the possibility of printing a Wallet paper, buying a cold wallet and storing it in the company\'s Secure safe.',
    //   autoload : true,
    //   page     : 'Home'
    // },
    // {
    //   key      : 'main_heading',
    //   type     : 'text',
    //   label    : 'Main Heading',
    //   content  : 'The best way to buy Cryptocurrency',
    //   autoload : true,
    //   page     : 'Home'
    // },
    {
      key      : 'buy_atm_text',
      type     : 'text',
      label    : 'Buy ATM Details Text',
      content  : 'Test Test Test Test Test Test',
      autoload : true,
      page     : 'Buy ATM Machines'
    }




  ];

    for(var i =0 ; i < s.length ; i++)
    {
      new Settings(s[i]).save().then().catch(function(){});
    }
   res.status(200).send('okkkkk');


}

exports.updateSettingsFile = function(req, res, next)
{

  // console.log(req.body.data)
  var data  = req.body.data;
  data = JSON.parse(data);
  if(req.file.path)
  {
    // console.log(req.file)
    var path = req.file.filename;
    var name = req.file.originalname;
    name     = name.split('.');
    name     = name[name.length-1];
    fs.renameSync('uploads/images/'+path, 'uploads/images/key_' + path + "."+name);

    var booking = new Settings().where(
    {
      key: data.key
    }).fetch().then(function(booking){
			if(!booking)
			{
				return res.status(400).send(
					{
						msg : 'Something went wrong while uploading File'
					});;
			}
			booking.save(
			{
				content : "key_" + path + "."+name,
			})
			.then(function(settings)
			{
				res.send(
				{
					settings : settings,
					msg     : 'Image uploaded successfully.'
				});
			}).catch(function(err)
			{
				res.status(400).send(
				{
					msg : 'Something went wrong while uploading File'
				});
			});
		}).catch(function(err)
			{
				res.status(400).send(
				{
					msg : 'Something went wrong while uploading File'
				});
			});
	}


}

exports.updateSettings = function(req, res, next) {
  req.assert('content'   , 'Content cannot be blank').notEmpty();

  var errors = req.validationErrors();
  if (errors) {
    return res.status(400).send(errors);
  }

  var settings = new Settings({ id: req.body.id });

  settings.save({
    // title   : req.body.title,
    content        : req.body.content,
    content_hebrew : req.body.content_hebrew
  });

  settings.fetch().then(function(settings) {
    res.send({ settings : settings, msg: 'settings has been updated.' });
  }).catch(function(err) {
    res.status(400).send({ msg : 'Something went wrong while updating the settings' });
  });
};

exports.addSettings = function(req, res, next) {
  req.assert('title'   , 'Title cannot be blank').notEmpty();

  var errors = req.validationErrors();

  if (errors) {
    return res.status(400).send(errors);
  }

  new Settings({
    // title   : req.body.tit
    content        : req.body.content,
    content_hebrew : req.body.content_hebrew
  }).save()
  .then(function(settings) {
      res.send({ ok:true , msg: 'New settings has been created successfully.' });
  })
  .catch(function(err) {
    console.log(err)
       return res.status(400).send({ msg: 'Something went wrong while created a new settings' });
   });
};


exports.listSettings = function(req, res, next) {


 new Settings( )
 .orderBy('id', 'DESC')
  .fetchAll()
  .then(function(settings) {
    if (!settings) {
      return res.status(200).send([]);
    }
    return res.status(200).send({ok:true , settings: settings.toJSON()});
  })
  .catch(function(err) {
    return res.status(200).send([]);
  });
};

exports.listSettingsDefault = function(req, res, next)
{


   new Settings( )
   .orderBy('id', 'DESC')
   .where('autoload', true)
    .fetchAll()
    .then(function(settings) {
      if (!settings) {
        return res.status(200).send([]);
      }
      return res.status(200).send({ok:true , settings: settings.toJSON()});
    })
    .catch(function(err) {
      return res.status(200).send([]);
    });
}

exports.listSingleSettings = function(req, res, next)
{
  new Settings( ).where('id', req.params.id)
   .fetch()
   .then(function(settings) {
     if (!settings) {
       return res.status(200).send({id : req.params.id, title: '' });
     }
     return res.status(200).send({ok:true , settings: settings.toJSON()});
   })
   .catch(function(err) {
     return res.status(400).send({id : req.params.id, title: '', msg: 'failed to fetch from db'});
   });

}
