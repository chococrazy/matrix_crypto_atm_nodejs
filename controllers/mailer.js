var nodemailer = require('nodemailer');
var transporter = nodemailer.createTransport({
  service: 'Mailgun',
  auth: {
    user: process.env.MAILGUN_USERNAME,
    pass: process.env.MAILGUN_PASSWORD
  }
});


exports.doMail = function(to, from, from_name, subject, msg){

  if(from_name == false)
  {
    from_name = 'Matrix Revolutions';
  }
  if(from == false)
  {
    from = 'account@matrixrevolutions.org';
  }


  var mailOptions = {
    from    : from_name + ' ' + '<'+ from + '>',
    to      : to,
    subject : subject,
    text    : msg
  };

  transporter.sendMail(mailOptions, function(err) {
    console.log(err);
  });
}
