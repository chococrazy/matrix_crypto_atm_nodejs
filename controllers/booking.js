var fs = require('fs');
var Booking = require('../models/Booking');
var Notification = require('../models/Notification');
var mailer = require('./mailer');




exports.listAll = function(req, res, next)
{

  new Booking()
    .orderBy('id', 'DESC')
    .fetchAll({withRelated:['user', 'atm']})
    .then(function(bookings)
    {
      if (!bookings)
      {
        return res.status(200).send([]);
      }
      return res.status(200).send(
      {
        ok       : true,
        bookings : bookings.toJSON()
      });
    })
    .catch(function(err)
    {
      return res.status(200).send([]);
    });
}


exports.list = function(req, res, next)
{

  new Booking()
  .orderBy('id', 'DESC')
    .where("user_id", req.user.id)
    .fetchAll({withRelated:[ 'atm']})
    .then(function(bookings)
    {
      if (!bookings)
      {
        return res.status(200).send([]);
      }
      return res.status(200).send(
      {
        ok       : true,
        bookings : bookings.toJSON()
      });
    })
    .catch(function(err)
    {
      return res.status(200).send([]);
    });
}

exports.invoice = function(req, res, next)
{
  // console.log(req.body.data)
  var data  = req.body.data;
  data = JSON.parse(data);
  console.log(req.files)
  var files = [];

  for(var i = 0 ; i < req.files.length;i++)
  {
    var path = req.files[i].filename;
    var name = req.files[i].originalname;
    name = name.split('.');
    name = name[name.length-1];
    fs.renameSync('uploads/invoices/'+path, 'uploads/invoices/'+data.booking_id+"_" + path + "."+name);
    files.push(data.booking_id+"_" + path + "."+name)


  }
  if(files.length)
  {
    // console.log(req.file)


    var booking = new Booking(
    {
      id: data.booking_id
    });

    if (booking)
    {
      booking.save(
      {
        invoice_path : JSON.stringify(files),
        status       : 'Completed'
      });

      booking.fetch({withRelated:['user']}).then(function(booking)
      {
        booking = booking.toJSON();
        res.send(
        {
          booking : booking,
          msg     : 'Booking Invoice has been uploaded.'
        });

        mailer.doMail(booking.user.email, false, false,'Booking Invoices uploaded', "Hi\n\nInvoice has been updated for your ATM visit booking via Matrix Revolutions\n\nRegards\nMatrix Revolutions");
      }).catch(function(err)
      {
        res.status(400).send(
        {
          msg : 'Something went wrong while uploading invoice'
        });
      });
    }
    else
    {
      res.status(400).send(
      {
        msg : 'Something went wrong while uploading invoice'
      });
    }
  }else{
    res.status(400).send(
    {
      msg : 'Something went wrong while uploading invoice'
    });
  }



}

exports.approve = function(req, res, next)
{

  var booking = new Booking(
  {
    id: req.body.id
  });

  if (booking)
  {
    booking.save(
    {
      status : 'Invited'
    });

    booking.fetch({withRelated:['user']}).then(function(booking)
    {
      booking = booking.toJSON();
      res.send(
      {
        booking : booking,
        msg     : 'Booking has been updated.'
      });


      mailer.doMail(booking.user.email, false, false,'Booking Status Updated', "Hi\n\nStatus for your ATM visit booking via Matrix Revolutions has been updated to 'Invited'.\n\nRegards\nMatrix Revolutions");

    }).catch(function(err)
    {
      res.status(400).send(
      {
        msg : 'Something went wrong while updating the Booking'
      });
    });
  }
  else
  {
    res.status(400).send(
    {
      msg : 'Something went wrong while updating the Booking'
    });
  }

}

exports.reject = function(req, res, next)
{

  var booking = new Booking(
  {
    id: req.body.id
  });
  if (booking)
  {
    booking.save(
    {
      status : 'Rejected'
    });

    booking.fetch({withRelated:['user']}).then(function(booking)
    {
      booking = booking.toJSON();
      res.send(
      {
        booking: booking,
        msg: 'Booking has been updated.'
      });
      mailer.doMail(booking.user.email, false, false,'ATM Visit Booking Rejected', "Hi\n\nYour ATM visit booking via Matrix Revolutions has been rejected by admin.\n\nRegards\nMatrix Revolutions");
    }).catch(function(err)
    {
      res.status(400).send(
      {
        msg : 'Something went wrong while updating the Booking'
      });
    });
  }
  else
  {
    res.status(400).send(
    {
      msg : 'Something went wrong while updating the Booking'
    });
  }

}

exports.cancel = function(req, res, next)
{

  var booking = new Booking(
  {
    id: req.body.id
  });
  if (booking)
  {
    booking.save(
    {
      status : 'Cancelled'
    });

    booking.fetch({withRelated:['user']}).then(function(booking)
    {
      booking = booking.toJSON();
      res.send(
      {
        booking: booking,
        msg: 'Booking has been updated.'
      });
      mailer.doMail(booking.user.email, false, false,'ATM Visit Booking Cancelled', "Hi\n\nYour ATM visit booking request ID "+req.body.id +" via Matrix Revolutions has been Cancelled by you.\n\nRegards\nMatrix Revolutions");
      mailer.doMail(process.env.CONTACT_TO, false, false,'ATM Visit Booking Cancelled', "Hi\n\nATM visit booking request ID "+req.body.id +" has been Cancelled by user.\n\nRegards\nMatrix Revolutions");

      new Notification({
        description : "Booking #"+req.body.id+" has been cancelled by user",
        user_id     : req.user.id,
        type        : "ORDER_CANCEL",
        object_id   : req.body.id,
      })
      .save()
      .then(function(obj){})
      .catch(function(er){});

    }).catch(function(err)
    {
      res.status(400).send(
      {
        msg : 'Something went wrong while updating the Booking'
      });
    });
  }
  else
  {
    res.status(400).send(
    {
      msg : 'Something went wrong while updating the Booking'
    });
  }

}

exports.complete = function(req, res, next)
{

  var booking = new Booking(
  {
    id: req.body.id
  });
  if (booking)
  {
    booking.save(
    {
      status : 'Completed'
    });

    booking.fetch({withRelated:['user']}).then(function(booking)
    {
      booking = booking.toJSON();
      res.send(
      {
        booking : booking,
        msg     : 'Booking has been updated.'
      });
      mailer.doMail(booking.user.email, false, false,'Booking Status Updated', "Hi\n\nStatus for your ATM visit booking via Matrix Revolutions has been updated to 'Completed'.\n\nRegards\nMatrix Revolutions");
    }).catch(function(err)
    {
      res.status(400).send(
      {
        msg: 'Something went wrong while updating the Booking'
      });
    });
  }
  else
  {
    res.status(400).send(
    {
      msg: 'Something went wrong while updating the Booking'
    });
  }

}

exports.new = function(req, res, next)
{
  req.assert('user_id'          , 'User cannot be blank').notEmpty();
  req.assert('listing_id'       , 'Listing cannot be blank').notEmpty();
  req.assert('date'             , 'Booking Date cannot be blank').notEmpty();
  req.assert('time'             , 'Booking Time cannot be blank').notEmpty();
  req.assert('transaction_type' , 'Transaction Type cannot be blank').notEmpty();
  req.assert('desired_amount'   , 'How much do you want to buy/sell').notEmpty();

  var errors = req.validationErrors();

  if (errors)
  {
    return res.status(400).send(errors);
  }

  new Booking(
    {
      user_id          : req.body.user_id,
      listing_id       : req.body.listing_id,
      booking_date     : req.body.date,
      desired_amount   : req.body.desired_amount,
      booking_time     : req.body.time,
      transaction_type : req.body.transaction_type,
    }).save()
    .then(function(atm)
    {
        res.send(
      {
        ok  : true,
        msg : 'Your Visit Booking Has been received. We will approve it soon.'
      });
      // console.log(req.user)
      mailer.doMail(req.user.get("email"), false, false,'New ATM visit booking', "Hi\n\nThankyou for your booking of visit for our Matrix ATMs.\n\nRegards\nMatrix Revolutions");

      mailer.doMail(process.env.CONTACT_TO, false, false, "New ATM visit Booking Received", "Hi there!\n\nYou have received a new booking visit request on MatrixATM.io \n\nPlease login to your admin dashboard to take action on the booking. \n\nRegards\nMatrix Revolutions");

      new Notification({
        description : "New Booking #"+atm.id+" has been received.",
        user_id     : req.user.id,
        type        : "ORDER_NEW",
        object_id   : atm.id,
      })
      .save()
      .then(function(obj){})
      .catch(function(er){});
      return;

    })
    .catch(function(err)
    {
      console.log(err);
      return res.status(400).send(
      {
        msg: 'Something went wrong while registering your Visit.'
      });
    });
}
