var fs = require('fs');
var async = require('async');
var crypto = require('crypto');
var nodemailer = require('nodemailer');
var jwt = require('jsonwebtoken');
var moment = require('moment');
var request = require('request');
var qs = require('querystring');
var User = require('../models/User');
var mailer = require('./mailer');

function generateToken(user) {
  var payload = {
    iss: 'my.domain.com',
    sub: user.id,
    iat: moment().unix(),
    exp: moment().add(7, 'days').unix()
  };
  return jwt.sign(payload, process.env.TOKEN_SECRET);
}


function sendMailToActivate(req, user){
  console.log(user.email);

  var link = 'https://' + req.headers.host + '/activate/' + user.id + '/'+moment(user.created_at).unix();

  mailer.doMail(user.email, false, false, 'Matrix - Verify your email',

  "Hi\n\nPlease click on the link below to verify your email and activate your Matrix Revolutions account:  \n\n"+link+"\n\n Regards")

}

/**
 * Login required middleware
 */
exports.ensureAuthenticated = function(req, res, next) {
  if (req.isAuthenticated()) {
    next();
  } else {
    res.status(401).send({ msg: 'Unauthorized' });
  }
};
  /**
   * POST /login
   * Sign in with email and password
   */

exports.loginPost = function(req, res, next) {
    req.assert('email', 'Email is not valid').isEmail();
    req.assert('email', 'Email cannot be blank').notEmpty();
    req.assert('password', 'Password cannot be blank').notEmpty();
    req.sanitize('email').normalizeEmail({ remove_dots: false });

    var errors = req.validationErrors();

    if (errors) {
      return res.status(400).send(errors);
    }

    new User({ email: req.body.email })
      .fetch()
      .then(function(user) {
        if (!user) {
          return res.status(401).send({ msg: 'The email address ' + req.body.email + ' is not associated with any account. ' +
          'Double-check your email address and try again.'
          });
        }
        if(user.get('status') == false)
        {
          return res.status(401).send({ msg: 'You have been banned by admin.'});
        }
        if(user.get('email_verified') == false)
        {
           res.status(401).send({ msg: 'You need to verify your email by clicking on the link we sent you. We have re-sent you the verification mail.'});

          sendMailToActivate(req, user.toJSON());
          return;
        }
        user.comparePassword(req.body.password, function(err, isMatch) {
          if (!isMatch) {
            return res.status(401).send({ msg: 'Invalid email or password' });
          }
          res.send({ token: generateToken(user), user: user.toJSON() });
        });
      }).catch(function(err){
        console.log(err)

      });
  };




exports.resend = function(req, res, next)
{
  var uid = req.query.uid;
  console.log(uid);
  new User({ id: uid })
  .fetch()
  .then(function(user) {
    if(!user)
    {

      res.status(400).send({ok:false});
    }
    sendMailToActivate(req, user.toJSON())
    res.status(200).send({ok:true});
    return;
  });

}


exports.signupPost = function(req, res, next) {
  req.assert('name', 'Name cannot be blank').notEmpty();
  req.assert('email', 'Email is not valid').isEmail();
  req.assert('email', 'Email cannot be blank').notEmpty();
  req.assert('location', 'Address cannot be blank').notEmpty();
	req.assert('country', 'Country cannot be blank').notEmpty();
	req.assert('city', 'City cannot be blank').notEmpty();
	req.assert('zipcode', 'zipcode cannot be blank').notEmpty();
	req.assert('phone_number', 'Phone Number cannot be blank').notEmpty();

  req.assert('password', 'Password must be at least 4 characters long').len(4);
  req.sanitize('email').normalizeEmail({ remove_dots: false });

  var errors = req.validationErrors();

  if (errors) {
    return res.status(400).send(errors);
  }

  new User({
    name         : req.body.name,
    email        : req.body.email,
    password     : req.body.password,
    phone_number : req.body.phone_number,
    city         : req.body.city,
    zipcode      : req.body.zipcode,
    location     : req.body.location,
    country      : req.body.country,
    role         : 'member'
  }).save()
    .then(function(user) {
        res.send({ msg: 'Your Account has been successfully created. To Activate your account, please click on the activation link in the mail we have sent you.', user : user.toJSON() });
        sendMailToActivate(req, user.toJSON())
        return;
    })
    .catch(function(err) {
      if (err.code === 'ER_DUP_ENTRY' || err.code === '23505') {
        return res.status(400).send({ msg: 'The email address you have entered is already associated with another account.' });
      }
    });
};


/**
 * PUT /account
 * Update profile information OR change password.
 */
exports.accountPut = function(req, res, next) {
  if ('password' in req.body) {
    req.assert('password', 'Password must be at least 4 characters long').len(4);
    req.assert('confirm', 'Passwords must match').equals(req.body.password);
  } else {
    req.assert('email' , 'Email is not valid').isEmail();
    req.assert('email' , 'Email cannot be blank').notEmpty();
    req.sanitize('email').normalizeEmail({ remove_dots: false });
  }

  var errors = req.validationErrors();

  if (errors) {
    return res.status(400).send(errors);
  }

  var user = new User({ id: req.user.id });
  if ('password' in req.body) {
    user.save({ password: req.body.password }, { patch: true });
  } else {
    user.save({
      email        : req.body.email,
      name         : req.body.name,
      gender       : req.body.gender,
      location     : req.body.location,
      country      : req.body.country,
      phone_number : req.body.phone_number,
      city         : req.body.city,
      dob          : moment(req.body.dob).format('YYYY-MM-DD'),
      zipcode      : req.body.zipcode
    }, { patch: true });
  }
  setTimeout(function(){


    user.fetch().then(function(user) {
      if ('password' in req.body) {
        res.send({ msg: 'Your password has been changed.' });
      } else {
        // user.dob   = new Date(user.dob);
        res.send({ user: user, msg: 'Your profile information has been updated.' });
      }
      res.redirect('/account');
    }).catch(function(err) {
      if (err.code === 'ER_DUP_ENTRY') {
        res.status(409).send({ msg: 'The email address you have entered is already associated with another account.' });
      }
    });
  }, 500);
};

/**
 * DELETE /account
 */
exports.accountDelete = function(req, res, next) {
  new User({ id: req.user.id }).destroy().then(function(user) {
    res.send({ msg: 'Your account has been permanently deleted.' });
  });
};

/**
 * GET /unlink/:provider
 */
exports.unlink = function(req, res, next) {
  new User({ id: req.user.id })
    .fetch()
    .then(function(user) {
      switch (req.params.provider) {
        case 'facebook':
          user.set('facebook', null);
          break;
        case 'google':
          user.set('google', null);
          break;
        case 'twitter':
          user.set('twitter', null);
          break;
        case 'vk':
          user.set('vk', null);
          break;
        default:
        return res.status(400).send({ msg: 'Invalid OAuth Provider' });
      }
      user.save(user.changed, { patch: true }).then(function() {
      res.send({ msg: 'Your account has been unlinked.' });
      });
    });
};

/**
 * POST /forgot
 */
exports.forgotPost = function(req, res, next) {
  req.assert('email', 'Email is not valid').isEmail();
  req.assert('email', 'Email cannot be blank').notEmpty();
  req.sanitize('email').normalizeEmail({ remove_dots: false });

  var errors = req.validationErrors();

  if (errors) {
    return res.status(400).send(errors);
  }

  async.waterfall([
    function(done) {
      crypto.randomBytes(16, function(err, buf) {
        var token = buf.toString('hex');
        done(err, token);
      });
    },
    function(token, done) {
      new User({ email: req.body.email })
        .fetch()
        .then(function(user) {
          if (!user) {
        return res.status(400).send({ msg: 'The email address ' + req.body.email + ' is not associated with any account.' });
          }
          user.set('passwordResetToken', token);
          user.set('passwordResetExpires', new Date(Date.now() + 3600000)); // expire in 1 hour
          user.save(user.changed, { patch: true }).then(function() {
            done(null, token, user.toJSON());
          });
        });
    },
    function(token, user, done) {
      res.send({ msg: 'An email has been sent to ' + user.email + ' with further instructions.' });

      var transporter = nodemailer.createTransport({
        service: 'Mailgun',
        auth: {
          user: process.env.MAILGUN_USERNAME,
          pass: process.env.MAILGUN_PASSWORD
        }
      });
      var mailOptions = {
        to: user.email,
        from: 'account@matrixrevolutions.com',
        subject: '✔ Reset your password on Matrix',
        text: 'You are receiving this email because you (or someone else) have requested the reset of the password for your account.\n\n' +
        'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
        'https://' + req.headers.host + '/reset/' + token + '\n\n' +
        'If you did not request this, please ignore this email and your password will remain unchanged.\n'
      };
      transporter.sendMail(mailOptions, function(err) {
          done(err);
      });
    }
  ]);
};

/**
 * POST /reset
 */
exports.resetPost = function(req, res, next) {
  req.assert('password', 'Password must be at least 4 characters long').len(4);
  req.assert('confirm', 'Passwords must match').equals(req.body.password);

  var errors = req.validationErrors();

  if (errors) {
      return res.status(400).send(errors);
  }

  async.waterfall([
    function(done) {
      new User({ passwordResetToken: req.params.token })
        .where('passwordResetExpires', '>', new Date())
        .fetch()
        .then(function(user) {
          if (!user) {
          return res.status(400).send({ msg: 'Password reset token is invalid or has expired.' });
          }
          user.set('password', req.body.password);
          user.set('passwordResetToken', null);
          user.set('passwordResetExpires', null);
          user.save(user.changed, { patch: true }).then(function() {
          done(null, user.toJSON());
          });
        });
    },
    function(user, done) {
      res.send({ msg: 'Your password has been changed successfully.' });
      var transporter = nodemailer.createTransport({
        service: 'Mailgun',
        auth: {
          user: process.env.MAILGUN_USERNAME,
          pass: process.env.MAILGUN_PASSWORD
        }
      });
      var mailOptions = {
        from: 'account@matrixrevolutions.com',
        to: user.email,
        subject: 'Your Matrix password has been changed',
        text: 'Hello,\n\n' +
        'This is a confirmation that the password for your account ' + user.email + ' has just been changed.\n'
      };
      transporter.sendMail(mailOptions, function(err) {

      });
    }
  ]);
};


exports.kyc = function(req, res, next)
{
  // console.log(req.files);
  var data  = req.body.data;
  data = JSON.parse(data);
  if(req.files)
  {

    var path_1 = req.files.file_1[0].filename;
    var name_1 = req.files.file_1[0].originalname;
    name_1     = name_1.split('.');
    name_1     = name_1[name_1.length-1];

   //var path_2 = req.files.file_2[0].filename;
    //var name_2 = req.files.file_2[0].originalname;
    //name_2     = name_2.split('.');
    //name_2     = name_2[name_2.length-1];

    var uid = req.user.id;

    var new_name_1 = uid+"_1_" + path_1 + "."+name_1;
    //var new_name_2 = uid+"_2_" + path_2 + "."+name_2;

    fs.renameSync('uploads/kyc/'+path_1, 'uploads/kyc/'+new_name_1);
   // fs.renameSync('uploads/kyc/'+path_2, 'uploads/kyc/'+new_name_2);

    var user = new User(
    {
      id: uid
    });

    if (user)
    {
      user.save(
      {
        kyc_1_text   : data.kyc_1_text,
        kyc_1_type   : data.kyc_1_type,
        kyc_1_path   : new_name_1,
        kyc_rejected : false,
        kyc_done     : true,
      }, { patch: true }).then(function(user)
      {
        user.fetch().
        then(function(user){
          res.send(
          {
            user : user.toJSON(),
            msg     : 'User Kyc Has been updated.'
          });
          mailer.doMail(process.env.CONTACT_TO, false, false, "New KYC Details Received", "Hi there!\n\nYou have received a new KYC approval request on MatrixATM.io \n\nUser's Name: "+user.get("name")+"\nEmail: "+user.get("email")+"\n\nPlease login to your admin dashboard to take action on the booking. \n\nRegards\nMatrix Revolutions");
        }).catch(function(){
          res.send(
          {
            msg     : 'User Kyc Has been updated.'
          });
        })

      }).catch(function(err)
      {
        res.status(400).send(
        {
          msg : 'Something went wrong while updating kyc!'
        });
      });
    }
    else
    {
      res.status(400).send(
      {
        msg : 'Something went wrong while updating kyc!'
      });
    }
  }else{
    res.status(400).send(
    {
      msg : 'Something went wrong while updating kyc!'
    });
  }




}


exports.listUsers = function(req, res, next) {


 new User( )
 .orderBy('id', 'DESC')
  .fetchAll()
  .then(function(users) {
    if (!users) {
      return res.status(200).send([]);
    }
    return res.status(200).send({ok:true , users: users.toJSON()});
  })
  .catch(function(err) {
    return res.status(200).send([]);
  });
};


exports.deleteUser = function(req, res, next)
{
	new User({ id: req.body.id }).destroy().then(function(post) {
    res.send({ msg: 'The User has been successfully deleted.' });
  }).catch(function(err) {
    return res.status(400).send({ msg : 'Something went wrong while deleting the User' });
  });

}


exports.approveUser = function(req, res, next)
{


  var user = new User({ id: req.body.id });

	if(user){
		user.save({
			kyc_approved : true,
      kyc_rejected : false
    }, { patch: true }).then(function(){
      user.fetch().then(function(user) {
  			res.send({ user : user, msg: 'User has been updated.' });
        mailer.doMail(user.get('email'), false, false,'KYC Approved', "Hi\n\nYour KYC request on matrixatm.io has been approved by admin. You can now initiate your ATM visit requests by logging into our website.\n\nRegards\nMatrix Revolutions");
  		}).catch(function(err) {
  			res.status(400).send({ msg : 'Something went wrong while updating the User' });
  		});

    }).catch(function(err) {
      res.status(400).send({ msg : 'Something went wrong while updating the User' });
    });
	}else{
		res.status(400).send({ msg : 'Something went wrong while updating the User' });
	}


}

exports.rejectUser = function(req, res, next)
{


  var user = new User({ id: req.body.id });

	if(user){
		user.save({
      kyc_approved : false,
      kyc_rejected : true
    }, { patch: true }).then(function(){
      user.fetch().then(function(user) {
  			res.send({ user : user, msg: 'User has been updated.' });
        mailer.doMail(user.get('email'), false, false,'KYC Rejected', "Hi\n\nYour KYC request on matrixatm.io has been reject by admin. You can re-submit the KYC for approval or contact admin if you think this is by mistake.\n\nRegards\nMatrix Revolutions");
  		}).catch(function(err) {
  			res.status(400).send({ msg : 'Something went wrong while updating the User' });
  		});

    }).catch(function(err) {
      res.status(400).send({ msg : 'Something went wrong while updating the User' });
    });
	}else{
		res.status(400).send({ msg : 'Something went wrong while updating the User' });
	}


}

exports.banUser = function(req, res, next)
{


  var user = new User({ id: req.body.id });

	if(user){
		user.save({
			status : false
		}, { patch: true }).then(function(){
      user.fetch().then(function(user) {
  			res.send({ user : user, msg: 'User has been updated.' });
  		}).catch(function(err) {
  			res.status(400).send({ msg : 'Something went wrong while updating the User' });
  		});

    }).catch(function(err) {
      res.status(400).send({ msg : 'Something went wrong while updating the User' });
    });
	}else{
		res.status(400).send({ msg : 'Something went wrong while updating the User' });
	}


}


exports.makeAdmin = function(req, res, next)
{


  var user = new User({ id: req.body.id });

	if(user){
		user.save({
			role : 'admin'
		}, { patch: true }).then(function(){
      user.fetch().then(function(user) {
  			res.send({ user : user, msg: 'User has been made admin.' });
  		}).catch(function(err) {
  			res.status(400).send({ msg : 'Something went wrong while updating the User' });
  		});

    }).catch(function(err) {
      res.status(400).send({ msg : 'Something went wrong while updating the User' });
    });
	}else{
		res.status(400).send({ msg : 'Something went wrong while updating the User' });
	}


}

exports.makeMember = function(req, res, next)
{


  var user = new User({ id: req.body.id });

	if(user){
		user.save({
			role : 'member'
		}, { patch: true }).then(function(){
      user.fetch().then(function(user) {
  			res.send({ user : user, msg: 'User has removed from admin list.' });
  		}).catch(function(err) {
  			res.status(400).send({ msg : 'Something went wrong while updating the User' });
  		});

    }).catch(function(err) {
      res.status(400).send({ msg : 'Something went wrong while updating the User' });
    });
	}else{
		res.status(400).send({ msg : 'Something went wrong while updating the User' });
	}


}

exports.activateUser = function(req,res, next)
{
  // console.log('514');
  new User({id:  req.params.id}).fetch().then(function(usr){
    // console.log('516 ',req.params );
    if(usr)
    {
      // console.log('519');
      if( moment(usr.get('created_at')).unix() == req.params.timestamp )
      {
        // console.log('522');

        usr.save({email_verified : true}, { patch: true }).then(function(){
          res.redirect('/login/successVerify');
        }).catch(function(err){
          console.log(err)
          res.status(200).send('unable to save, please contact admin');
        })

      }else
      {
        res.status(200).send('invalid link');

      }

    }else{
      res.status(200).send('invalid user link');

    }
  }).catch(function(err){
    console.log(err)
    res.status(200).send('unable to fetch, please contact admin');
  })

}

exports.unbanUser = function(req, res, next)
{


  var user = new User({ id: req.body.id });

	if(user){
		user.save({
			status : true
		}, { patch: true }).then(function(){
      user.fetch().then(function(user) {
  			res.send({ user : user, msg: 'User has been updated.' });
  		}).catch(function(err) {
  			res.status(400).send({ msg : 'Something went wrong while updating the User' });
  		});

    }).catch(function(err) {
      res.status(400).send({ msg : 'Something went wrong while updating the User' });
    });
	}else{
		res.status(400).send({ msg : 'Something went wrong while updating the User' });
	}


}
