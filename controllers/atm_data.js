var MODELS = require('../models/atm_data/Bills');


exports.all_atm_data = function(req, res, next) {

  switch(req.params.type)
  {



    case 'Bills':
      var m = new MODELS.Bills(); break;
    case 'CashInActions':
      var m = new MODELS.CashInActions();break;
    case 'CashInRefills':
      var m = new MODELS.CashInRefills();break;
    case 'CashInTxs':
      var m = new MODELS.CashInTxs();break;
    case 'CashOutActions':
      var m = new MODELS.CashOutActions();break;
    case 'CashOutRefills':
      var m = new MODELS.CashOutRefills();break;
    case 'CashOutTxs':
      var m = new MODELS.CashOutTxs();break;
    case 'ComplianceOverrides':
      var m = new MODELS.ComplianceOverrides();break;
    case 'Customers':
      var m = new MODELS.Customers();break;
    case 'Devices':
      var m = new MODELS.Devices();break;
    case 'Trades':
      var m = new MODELS.Trades();break;


  }

  if(m)
  {
    m.fetchAll().then(function(data){
      if(data){
        res.status(200).send({ok:true, data : data.toJSON()});
      }else{
        res.status(200).send({ok:true, data :  [] });
      }
      return;
    }).catch(function(err){
      res.status(200).send({ok:true, data : []});
    });
  }else{
    res.status(200).send({ok:true, data : []});
  }



};
