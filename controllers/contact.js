var MachineRequest = require('../models/MachineRequest');

var nodemailer = require('nodemailer');
var transporter = nodemailer.createTransport({
  service: 'Mailgun',
  auth: {
    user: process.env.MAILGUN_USERNAME,
    pass: process.env.MAILGUN_PASSWORD
  }
});

/**
 * GET /contact
 */
exports.contactGet = function(req, res) {
  res.render('contact', {
    title: 'Contact'
  });
};


exports.contactATMPost = function(req, res) {

  req.assert('first_name', 'First Name cannot be blank').notEmpty();
  req.assert('last_name',  'Last Name cannot be blank').notEmpty();
  req.assert('email',      'Email is not valid').isEmail();
  req.assert('email',      'Email cannot be blank').notEmpty();
  req.assert('message',    'Message cannot be blank').notEmpty();
  req.assert('company',    'Company cannot be blank').notEmpty();

  req.sanitize('email').normalizeEmail({ remove_dots: false });

  var errors = req.validationErrors();

  if (errors) {
    return res.status(400).send(errors);
  }

  res.send({ msg: 'Thank you! We will contact you soon.' });

  new MachineRequest({
    first_name : req.body.first_name,
    last_name  : req.body.last_name,
    email      : req.body.email,
    company    : req.body.company,
    msg        : req.body.message

  })
  .save()
  .then(function(obj){})
  .catch(function(er){console.log(er)});

  var mailOptions = {
    from: req.body.first_name + " "+req.body.last_name + ' ' + '<'+ req.body.email + '>',
    to: process.env.CONTACT_TO,
    subject: 'New Submission in \'Buy ATM Machine\' Form ',
    text: "Company: "+req.body.company+"\n\n"+"Message: "+req.body.message
  };


  transporter.sendMail(mailOptions, function(err) {
    console.log(err);

  });
};



exports.contactPost = function(req, res) {
  req.assert('name', 'Name cannot be blank').notEmpty();
  req.assert('email', 'Email is not valid').isEmail();
  req.assert('email', 'Email cannot be blank').notEmpty();
  req.assert('message', 'Message cannot be blank').notEmpty();
  req.assert('subject', 'Subject cannot be blank').notEmpty();
  req.sanitize('email').normalizeEmail({ remove_dots: false });

  var errors = req.validationErrors();

  if (errors) {
    return res.status(400).send(errors);
  }

  var mailOptions = {
    from: req.body.name + ' ' + '<'+ req.body.email + '>',
    to: process.env.CONTACT_TO,
    subject: 'Contact Form: '+req.body.subject,
    text: "Phone: "+req.body.phone+"\n\n"+"Message: "+req.body.message
  };
  res.send({ msg: 'Thank you! Your feedback has been submitted.' });

  transporter.sendMail(mailOptions, function(err) {
    console.log(err);

  });
};






exports.listAll = function(req, res, next)
{

  new MachineRequest()
    .orderBy('id', 'DESC')
    .fetchAll( )
    .then(function(bookings)
    {
      if (!bookings)
      {
        return res.status(200).send([]);
      }
      return res.status(200).send(
      {
        ok   : true,
        rows : bookings.toJSON()
      });
    })
    .catch(function(err)
    {
      return res.status(200).send({ok:true, rows: []});
    });
}
