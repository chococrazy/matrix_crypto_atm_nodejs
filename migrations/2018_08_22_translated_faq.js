exports.up = function(knex, Promise) {
  return Promise.all([

    knex.schema.table('faq', function(table) {
      table.string('title_he');
      table.text('content_he');
      table.string('category_he');
      }),
      knex.schema.table('category', function(table) {
        table.string('title_he');
      }),

      knex.schema.table('blogpost', function(table) {
        table.string('title_he');
        table.string('content_he');
        table.string('short_content_he');
      }),

      knex.schema.table('atms', function(table) {
        table.string('title_he');
        table.string('description_he');
        table.string('address_he');
      }),
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([


  ])
};
