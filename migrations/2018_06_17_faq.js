exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTable('faq', function(table) {
      table.increments();
      table.string('title');
      // table.string('title_he');
      table.text('content');
      // table.text('content_he');
      table.string('category');
      table.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTable('faq')
  ])
};
