exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTable('settings', function(table) {
      table.increments();
      table.string('key').unique();
      table.string('type');
      table.text('content');
      table.string('page');
      table.string('label');
      table.string('content_hebrew');
      table.boolean('autoload');

    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTable('settings')
  ])
};
