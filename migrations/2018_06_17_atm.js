exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTable('atms', function(table) {
      table.increments();
      table.string('title');
      table.text('description');
      table.string('address');
      table.string('lat');
      table.string('long');
      table.boolean('has_buy_option');
      table.boolean('has_sell_option');
      table.time('start_time');
      table.time('close_time');
      table.string('days_available');
      table.string('photo_path');
      table.string('phone_number');
      table.string('commission_sell');
      table.string('commission');
      table.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTable('atms')
  ])
};
