exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTable('users', function(table) {
      table.increments();
      table.string('name');
      table.string('email').unique();
      table.string('password');
      table.string('gender');
      table.string('passwordResetToken');
      table.dateTime('passwordResetExpires');
      table.string('location');
      table.string('role').defaultTo("member");
      table.string('kyc_1_text');
      table.string('kyc_1_path');
      table.string('kyc_1_type');
      table.string('kyc_2_text');
      table.string('kyc_2_path');
      table.boolean('kyc_done').defaultTo(false);
      table.boolean('kyc_approved').defaultTo(false);
      table.string('phone_number');
      table.string('city');
      table.string('zipcode');
      table.string('country');
      table.string('dob');
      table.boolean('status').defaultTo(true);
      table.boolean('email_verified').defaultTo(false);
      table.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTable('users')
  ])
};
