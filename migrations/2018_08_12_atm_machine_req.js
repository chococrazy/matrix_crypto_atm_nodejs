exports.up = function(knex, Promise) {
  return Promise.all([
  knex.schema.createTable('machine_requests', function(table) {
    table.increments();
    table.string('first_name');
    table.string('last_name');
    table.string('email');
    table.string('company');
    table.text('msg');
    table.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTable('machine_requests')

  ])
};
