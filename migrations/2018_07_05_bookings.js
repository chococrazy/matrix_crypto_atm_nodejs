exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTable('bookings', function(table) {
      table.increments();
      // table.string('key').unique();
      table.integer('user_id');
      table.integer('listing_id');
      table.string('booking_date');
      table.string('booking_time');
      table.string('transaction_type');
      table.string('invoice_path');
      table.string('desired_amount');
      table.string('status').defaultTo("pending_approval");
      table.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTable('bookings')
  ])
};
