var fs      = require('fs');
var express = require('express');
var http    = require('http');
var https   = require('https');

var path             = require('path');
var logger           = require('morgan');
var compression      = require('compression');
var cookieParser     = require('cookie-parser');
var bodyParser       = require('body-parser');
var expressValidator = require('express-validator');
var dotenv           = require('dotenv');
var jwt              = require('jsonwebtoken');
var moment           = require('moment');
var request          = require('request');

var multer         = require('multer');
var uploadInvoices = multer({ dest: 'uploads/invoices' })
var uploadKYC      = multer({ dest: 'uploads/kyc' })
var uploadIMG      = multer({ dest: 'uploads/images' })
var cpUpload       = uploadKYC.fields([{ name: 'file_1', maxCount: 1 } ])

// Load environment variables from .env file
dotenv.load();

// Models
var User = require('./models/User');

// Controllers
var userController     = require('./controllers/user');
var contactController  = require('./controllers/contact');
var contactController  = require('./controllers/contact');
var faqController      = require('./controllers/faq');
var atmController      = require('./controllers/atm');
var blogController     = require('./controllers/post');
var cryptController    = require('./controllers/crypt');
var categoryController = require('./controllers/category');
var settingsController = require('./controllers/settings');
var bookingController  = require('./controllers/booking');
var mediaController    = require('./controllers/media');
var graphController    = require('./controllers/graph_for_tv')
var notifController = require('./controllers/notif')
var atmdataController = require('./controllers/atm_data')


var app = express();


app.use(function (req, res, next) {
  var str = "www.";
  var h = req.hostname ? req.hostname : req.host;
  if(!h)
    h= req.headers.host;
  if(!h)
    h='';
  if (h.indexOf(str) >= 0) {
    console.log('lets redirect');
    res.redirect(301, req.protocol + "://matrixatm.io"  + req.originalUrl);
  } else {
    next();
  }
});



app.set('port', process.env.PORT || 80);
app.set('port_ssl', process.env.PORT_SSL || 443);
app.use(compression());
// app.use(logger('dev'));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(expressValidator());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use("/downloads/invoices",express.static(path.join(__dirname, 'uploads/invoices')));
app.use("/downloads/kyc",express.static(path.join(__dirname, 'uploads/kyc')));
app.use("/downloads/images",express.static(path.join(__dirname, 'uploads/images')));
app.use("/graph/",express.static(path.join(__dirname, 'graph')));
app.use("/graph_old/",express.static(path.join(__dirname, 'graph_old')));
console.log(process.env.TOKEN_SECRET);
console.log(' -- - -- --- --- ')
app.use(function(req, res, next) {
  req.isAuthenticated = function() {
    var token = (req.headers.authorization && req.headers.authorization.split(' ')[1]) || req.cookies.token;
    try {
      return jwt.verify(token, process.env.TOKEN_SECRET);
    } catch (err) {
      // console.log(err);
      return false;
    }
  };

  if (req.isAuthenticated()) {
    var payload = req.isAuthenticated();
    new User({ id: payload.sub })
      .fetch()
      .then(function(user) {
        if(!user)
        {
          next();
          return;
        }
        req.user = user;
        next();
      });
  } else {
    next();
  }
});

app.post('/contact'     , contactController.contactPost);
app.post('/contactATM'  , contactController.contactATMPost);
app.get('/contact_list' , userController.ensureAuthenticated , contactController.listAll);

app.get('/me'            , userController.ensureAuthenticated , function(req, res, next){
  console.log()
  res.status(200).send({
    user : req.user.toJSON(),
    ok   : true
  });
});
app.put('/account'       , userController.ensureAuthenticated , userController.accountPut);
app.delete('/account'    , userController.ensureAuthenticated , userController.accountDelete);
app.post('/signup'       , userController.signupPost);
app.get('/resend'       , userController.resend);
app.post('/login'        , userController.loginPost);
app.post('/forgot'       , userController.forgotPost);
app.post('/reset/:token' , userController.resetPost);
app.get('/user/list'     , userController.ensureAuthenticated , userController.listUsers );
app.post('/user/approve' , userController.ensureAuthenticated , userController.approveUser );
app.post('/user/reject'  , userController.ensureAuthenticated , userController.rejectUser );
app.post('/user/delete'  , userController.ensureAuthenticated , userController.deleteUser );
app.post('/user/ban'     , userController.ensureAuthenticated , userController.banUser );
app.post('/user/unban'   , userController.ensureAuthenticated , userController.unbanUser );

app.post('/user/makeAdmin'         , userController.ensureAuthenticated , userController.makeAdmin );
app.post('/user/makeMember'        , userController.ensureAuthenticated , userController.makeMember );


app.get('/activate/:id/:timestamp' , userController.activateUser );

app.post('/profile/kyc' , userController.ensureAuthenticated ,cpUpload,  userController.kyc );



app.get('/unlink/:provider' , userController.ensureAuthenticated , userController.unlink);

app.get('/faq/list'       , faqController.listFaq );
app.get('/faq/single/:id' , faqController.listSingleFaq );
app.post('/faq/add'       , userController.ensureAuthenticated , faqController.addFaq );
app.post('/faq/edit'      , userController.ensureAuthenticated , faqController.updateFaq );
app.post('/faq/delete'    , userController.ensureAuthenticated , faqController.deleteFaq );


app.get('/blog/list'       , blogController.listBlogPost );
app.get('/blog/single/:id' , blogController.listSingleBlogPost );
app.post('/blog/add'       , userController.ensureAuthenticated , blogController.addBlogPost );
app.post('/blog/edit'      , userController.ensureAuthenticated , blogController.updateBlogPost );
app.post('/blog/edit/file'      , userController.ensureAuthenticated ,  uploadIMG.single('file')    , blogController.updateBlogPostImage );
app.post('/media/file'      , userController.ensureAuthenticated ,  uploadIMG.single('file')    , mediaController.file );


app.post('/blog/delete'    , userController.ensureAuthenticated , blogController.deleteBlogPost );


app.get('/atm/list'         , atmController.listAtm );
app.post('/atm/list/search' , atmController.listAtmSearch );

app.get('/atm/single/:id' , atmController.listSingleAtm );
app.post('/atm/add'       , userController.ensureAuthenticated , atmController.addAtm );
app.post('/atm/edit'      , userController.ensureAuthenticated , atmController.updateAtm );
app.post('/atm/delete'    , userController.ensureAuthenticated , atmController.deleteAtm );


app.get('/booking/list'      , userController.ensureAuthenticated , bookingController.list );
app.get('/booking/list/all'  , userController.ensureAuthenticated , bookingController.listAll );
app.post('/booking/new'      , userController.ensureAuthenticated , bookingController.new );
app.post('/booking/approve'  , userController.ensureAuthenticated , bookingController.approve );
app.post('/booking/reject'   , userController.ensureAuthenticated , bookingController.reject );
app.post('/booking/cancel'   , userController.ensureAuthenticated , bookingController.cancel );
app.post('/booking/complete' , userController.ensureAuthenticated , bookingController.complete );
app.post('/booking/invoice'  , userController.ensureAuthenticated , uploadInvoices.array('file')  , bookingController.invoice );

app.get('/fetchGraph' , graphController.price_of_listings);

app.get('/category/list'       , categoryController.listCategory );
app.get('/settings/list'       , settingsController.listSettings );
app.get('/settings/list/default'       , settingsController.listSettingsDefault );
app.get('/settings/single/:id' , settingsController.listSingleSettings );
app.post('/settings/edit'      , userController.ensureAuthenticated  ,  settingsController.updateSettings );
app.post('/settings/edit/file'      , userController.ensureAuthenticated  ,     uploadIMG.single('file')    , settingsController.updateSettingsFile );
app.get('/category/single/:id' , categoryController.listSingleCategory );
app.post('/category/add'       , userController.ensureAuthenticated       , categoryController.addCategory );
app.post('/category/edit'      , userController.ensureAuthenticated       , categoryController.updateCategory );
app.post('/category/delete'    , userController.ensureAuthenticated       , categoryController.deleteCategory );

app.post('/fetchPrice'    , cryptController.price)
app.post('/fetchPriceBTC' , cryptController.fetchPriceBTC)
app.get('/create_SATOSHIS' , settingsController.create_SATOSHIS)

app.get('/notifs/list', userController.ensureAuthenticated, notifController.list);
app.post('/notifs/delete', userController.ensureAuthenticated, notifController.delete);

// app.post('/settings/fetch' , settingsController.create_SATOSHIS)


app.get('/all_atm_data/:type', atmdataController.all_atm_data);

app.get('/admin_panel*', function(req, res) {
  // console.log(req.originalUrl)
  res.redirect('/admin_panel/#' + req.originalUrl.replace("/admin_panel/", ""));
});
app.get('/atm_data_panel*', function(req, res) {
  // console.log(req.originalUrl)
  res.redirect('/atm_data_panel/#' + req.originalUrl.replace("/atm_data_panel/", ""));
});
app.get('/*', function(req, res) {
  res.redirect('/#' + req.originalUrl);
});

// Production error handler
if (app.get('env') === 'production') {
  app.use(function(err, req, res, next) {
    console.error(err.stack);
    res.sendStatus(err.status || 500);
  });
}

http.createServer(function (req, res) {
    res.writeHead(301, { "Location": "https://" + req.headers['host'] + req.url });
    res.end();
}).listen(app.get('port'), function() {
  console.log('Express server listening on port ' + app.get('port'));
});
https.createServer({
  key: fs.readFileSync(__dirname+'/certs/csr/matrixatm_io.key'),
  cert: fs.readFileSync(__dirname+'/certs/ssl/matrixatm_io.crt'),
  ca: [
            fs.readFileSync('certs/ssl/matrixatm_io.ca-bundle' ),
        ]
  }, app).listen(app.get('port_ssl'), function() {
  console.log('Express server listening on port ' + app.get('port_ssl'));
});
//
//
// app.listen(3000, function() {
//   console.log('Express server listening on port ' + app.get('port'));
// });

module.exports = app;
