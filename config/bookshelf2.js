var config2 = require('../knexfile2');
var knex2 = require('knex')(config2);
var bookshelf2 = require('bookshelf')(knex2);

bookshelf2.plugin('virtuals');
bookshelf2.plugin('visibility');

//knex.migrate.latest();

module.exports = bookshelf2;
