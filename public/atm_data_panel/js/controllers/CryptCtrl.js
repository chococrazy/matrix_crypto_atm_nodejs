angular.module('MyApp')
.controller('HomeCtrl', function($scope, $location,$routeParams, $http){

  $scope.type = $routeParams.type;

  $scope.doSmart = function(r)
  {
    r = r.split("_");
    r = r.join(' ');
    return r;

  }
  $scope.tbl_name =  $scope.type;

$scope.loading =false;
  $scope.list = [];
  $scope.cols = [];
  $scope.fetch = function()
  {
    $scope.loading =true;
    $http.get('/all_atm_data/'+$scope.type).then(function(response){
        if(response.data.ok)
        {
          $scope.loading =false;
          $scope.list = response.data.data;
          if($scope.list.length){
            $scope.cols = Object.keys(response.data.data[0]);
          }
          else{
            $scope.cols = [];
          }
          setTimeout(function(){
            $('#datatables').DataTable({
              "pagingType": "full_numbers","order": [[ 0, 'desc' ]],
              responsive: true,
          });
        }, 300);
        }else{
          $scope.loading =false;
          $scope.cols = [];
          $scope.list = [];
        }
    })
    .catch(function(){
        $scope.loading =false;
        $scope.cols = [];
        $scope.list = [];
    })
  }
  $scope.fetch();
//
//   $scope.notifs = [ ];
//   $scope.show = false;
//
//   $scope.list = function(){
//     $scope.show = false;
//     NOTIFS.send()
//     .then(function(response) {
//       // console.log(response)
//       $scope.show = true;
//         $scope.notifs = response.data.notifs;
//     })
//     .catch(function(response) {
//         $scope.notifs = [ ];
//         $scope.show = true;
//       $scope.messages = {
//         error: Array.isArray(response.data) ? response.data : [response.data]
//       };
//     });
//   }
//   $scope.list();
//   $scope.delete = function(id)
//   {
//
//       NOTIFS.delete({id:id})
//       .then(function(response) {
//         $scope.messages = {
//           success: [response.data]
//         };
//         $scope.list();
//       })
//       .catch(function(response) {
//         // $scope.messages = {
//         //   error: Array.isArray(response.data) ? response.data : [response.data]
//         // };
//       });
//   }
//
//
//
// })
//
//
// .controller('AtmMachineCtrl', function($scope, Contact){
//
//   $scope.rows = [ ];
//   $scope.show = false;
//
//   $scope.list = function(){
//     $scope.show = false;
//     Contact.list()
//     .then(function(response) {
//       // console.log(response)
//       $scope.show = true;
//         $scope.rows = response.data.rows;
//     })
//     .catch(function(response) {
//         $scope.rows = [ ];
//         $scope.show = true;
//       $scope.messages = {
//         error: Array.isArray(response.data) ? response.data : [response.data]
//       };
//     });
//   }
//   $scope.list();
//   $scope.delete = function(id)
//   {
//
//       NOTIFS.delete({id:id})
//       .then(function(response) {
//         $scope.messages = {
//           success: [response.data]
//         };
//         $scope.list();
//       })
//       .catch(function(response) {
//         // $scope.messages = {
//         //   error: Array.isArray(response.data) ? response.data : [response.data]
//         // };
//       });
//   }
//
//
//
// })
//
//
//
//
//   .controller('CryptCtrl', function($scope, CRYPT)
//   {
//
//     $scope.currencies = {
//       GBP : 000,
//       ILS : 000,
//       USD : 000
//     }
//
//     $scope.fetchPRice = function(idx, id)
//     {
//       CRYPT.price(id)
//         .then(function(response)
//         {
//           if (response.data && response.data.data)
//           {
//             $scope.currencies[id] = Math.round(response.data.data.quotes[id].price * 100) / 100;
//
//           }
// 					console.log($scope.currencies)
//
//           // $scope.blogposts = response.data.posts;
//         })
//         .catch(function(response)
//         {
//           console.log(response);
//           $scope.blogposts = [];
//           $scope.messages = {
//             error: Array.isArray(response.data) ? response.data : [response.data]
//           };
//         });
//     }
//     // for (var i = 0; i < 3; i++)
//     // {
//     $scope.fetchPRice(0, "USD");
//     $scope.fetchPRice(0, "GBP");
//     $scope.fetchPRice(0 , "ILS");
//     // }
  });
