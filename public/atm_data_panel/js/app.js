angular.module('MyApp', ['ngRoute', 'satellizer'])
  .config(function($routeProvider, $locationProvider, $authProvider)
  {
    $locationProvider.html5Mode(true);

    $routeProvider
      .when('/:type',
      {
      controller: 'HomeCtrl',
        resolve:
        {
          loginRequired: loginRequired,

        },
        templateUrl: 'partials/home.html'
      })
     .otherwise(
      {
        templateUrl: 'partials/404.html'
      });

    $authProvider.loginUrl = '/login';
    $authProvider.signupUrl = '/signup';

    function skipIfAuthenticated($location, $auth)
    {
      if ($auth.isAuthenticated())
      {
        $location.path('/');
      }
    }

    function loginRequired($location, $auth, $http, $window)
    {

      if (!$auth.isAuthenticated())
      {
        window.location.href = window.location.origin + "/login"
      }

      $http.get('/me').then(function(res){
        if(res.data && res.data.user)
        {

          if(res.data.user.status  == false)
          {
            $auth.logout();
            delete $window.localStorage.user;
            window.location.href =  window.location.origin ;
          }

          // $rootScope.currentUser = response.data.user;
          // $window.localStorage. = ;
          localStorage.setItem('user', JSON.stringify(res.data.user));


          if(res.data.user.role != 'admin')
          {
            window.location.href = window.location.origin + "/account"
          }

        }else{
          window.location.href = window.location.origin + "/account"
        }

      })
      .catch(function(er){
        console.log(er)
          window.location.href = window.location.origin + "/login"
      })



    }
  })
  .run(function($rootScope, $window, $location)
  {
    if ($window.localStorage.user)
    {
      $rootScope.currentUser = JSON.parse($window.localStorage.user);
    }
    $rootScope.urlhas = function(f)
    {
      return (($location['$$path']).indexOf(f) >= 0);
    }
  });
