angular.module('MyApp')
  .controller('BookingsCtrl' , function($scope , $location , $window , $auth , Bookings , SweetAlert , $http)
  {
    $scope.bookings = [];


    $scope.list = function()
    {
      $scope.show = false;

      Bookings.list()
        .then(function(response)
        {
          $scope.show = true;
          $scope.bookings = response.data.bookings;
          setTimeout(function(){
            $('#datatables').DataTable({
              "pagingType": "full_numbers","order": [[ 0, 'desc' ]],
              responsive: true,
          });
          $('#datatables2').DataTable({
            "pagingType": "full_numbers","order": [[ 0, 'desc' ]],
            responsive: true,
        });
        $('#datatables3').DataTable({
          "pagingType": "full_numbers","order": [[ 0, 'desc' ]],
          responsive: true,
      });
        }, 300);
        })
        .catch(function(response)
        {
          $scope.show = true;
          $scope.bookings = [];
          $scope.messages = {
            error: Array.isArray(response.data) ? response.data : [response.data]
          };
        });
    }

    $scope.list();

    $scope.approve = function(id)
    {

        SweetAlert.swal({
          title: "Are you sure?",
          text: "Are you sure you want to perform this action?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes, do it!",
          closeOnConfirm: false
        },  function(e){
          if(!e) return;

        Bookings.approve(
          {
            id: id
          })
          .then(function(response)
          {
            $scope.messages = {
              success: [response.data]
            };
            setTimeout(function(){ $scope.list(); }, 200);
          })
          .catch(function(response)
          {
            $scope.messages = {
              error: Array.isArray(response.data) ? response.data : [response.data]
            };
            setTimeout(function(){ $scope.list(); }, 200);
          });

      });
    }

    $scope.goodStatus = function(s)
    {
      switch (s)
      {
        case 'pending_approval':
          return 'Pending Approval';
        case 'approved':
          return 'Approved';
        case 'rejected':
          return 'Rejected';

          break;
        default:
          return s;

      }

    }

    $scope.reject = function(id)
    {

        SweetAlert.swal({
          title: "Are you sure?",
          text: "Are you sure you want to perform this action?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes, do it!",
          closeOnConfirm: false
        },  function(e){
          if(!e) return;

        Bookings.reject(
          {
            id: id
          })
          .then(function(response)
          {
            $scope.messages = {
              success: [response.data]
            };
            setTimeout(function(){ $scope.list(); }, 200);
          })
          .catch(function(response)
          {
            $scope.messages = {
              error: Array.isArray(response.data) ? response.data : [response.data]
            };
            setTimeout(function(){ $scope.list(); }, 200);
          });

      });
    }



    $scope.uploadedFile = function(element)
    {
      // console.log(jQuery(element).attr('data-id_booking'))
      $scope.file_upload_booking_id = parseInt(jQuery(element).attr('data-id_booking'));
      for(var i = 0 ; i < $scope.bookings.length ; i++)
      {
        if($scope.bookings[i].id == $scope.file_upload_booking_id)
        {
          $scope.bookings[i].uploading = true;
          break;
        }
      }
      $scope.file_upload_started = true;
      $scope.$apply(function($scope)
      {
        $scope.files = element.files;
      });
      $scope.addFile();
    }

    $scope.addFile = function()
    {
      $scope.uploadfile($scope.files);
    }

    $scope.complete = function(id)
    {
        SweetAlert.swal({
          title: "Are you sure?",
          text: "Are you sure you want to perform this action?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes, do it!",
          closeOnConfirm: false
        },  function(e){
          if(!e) return;

        Bookings.complete(
          {
            id: id
          })
          .then(function(response)
          {
            $scope.file_upload_started = false;
            $scope.messages = {
              success: [response.data]
            };
            setTimeout(function(){ $scope.list(); }, 200);
          })
          .catch(function(response)
          {
            $scope.file_upload_started = false;
            $scope.messages = {
              error: Array.isArray(response.data) ? response.data : [response.data]
            };
            setTimeout(function(){ $scope.list(); }, 200);
          });

      });
    }

$scope.ppuur  = function(a)
{
  var separator = '...';
  var separatorlength = separator.length ;
  var maxlength = 10 - separatorlength;


  var start = maxlength / 2 ;
  var trunc =  a.length - maxlength;
  var b = a.substring(start, trunc);
  return a.replace(b, separator);


}




    $scope.pur = function(r)
    {
      return JSON.parse(r);
    }

    $scope.uploadfile = function(files)
    {

      var fd = new FormData();

      var url = '/booking/invoice';

      angular.forEach(files, function(file)
      {
        fd.append('file', file);
      });

      //sample data
      var data = {booking_id : $scope.file_upload_booking_id};

      fd.append("data", JSON.stringify(data));

      $http.post(url, fd,
        {
          withCredentials: false,
          headers:
          {
            'Content-Type': undefined
          },
          transformRequest: angular.identity
        })
        .success(function(data)
        {
          // console.log(data);
          $scope.messages = {
            success: [data]
          };
          setTimeout(function(){ $scope.list(); }, 200);
        })
        .error(function(data)
        {
          // console.log(data);
          $scope.messages = {
            error: [data]
          };
        });
    };

  });
