angular.module('MyApp')
  .controller('AtmCtrl', function($scope, $location, $window, $auth, ATM, SweetAlert) {
    $scope.atms = [ ];
    $scope.show = false;
    $scope.list = function(){
      $scope.show = false;

      ATM.list()
      .then(function(response) {
        $scope.show = true;
        // console.log(response)
          $scope.atms = response.data.atms;
          setTimeout(function(){
            $('#datatables').DataTable({
              "pagingType": "full_numbers",
              "order": [[ 0, 'desc' ]],
              responsive: true,
          });
        }, 300);
      })
      .catch(function(response) {
          $scope.atms = [ ];
          $scope.show = true;
        $scope.messages = {
          error: Array.isArray(response.data) ? response.data : [response.data]
        };
      });
    }
    $scope.list();


    $scope.requestToDelete = function(id)
    {

      SweetAlert.swal({
  			title: "Are you sure?",
  			text: "Are you sure you want to delete this?",
  			type: "warning",
  			showCancelButton: true,
  			confirmButtonColor: "#DD6B55",
  			confirmButtonText: "Yes, delete it!",
  			closeOnConfirm: false
  		},  function(e){
        if(!e) return;

        ATM.delete({id : id})
        .then(function(response) {
          $scope.messages = {
            success: [response.data]
          };
          $scope.list();
        })
        .catch(function(response) {
          $scope.messages = {
            error: Array.isArray(response.data) ? response.data : [response.data]
          };
        });
      });
    }


  });


angular.module('MyApp')
  .controller('AtmAddCtrl', function($scope, $location, $window, $auth, ATM) {
    $scope.atm = {title: '', description: '', days_available: '', has_buy_option : "no", has_sell_option : "no"}
    $scope.days = {
      day_1: false,
      day_2: false,
      day_3: false,
      day_4: false,
      day_5: false,
      day_6: false,
      day_0: false,
      };



    $scope.title = 'New ATM';
    $scope.submitForm = function(){
      if($scope.atm.title == '' || $scope.atm.description == '')
        return;
        var tmp =  [];
        for(var i  = 0 ; i <= 7 ; i++)
        {
          if( $scope.days['day_'+i])
            tmp.push(i);
        }
        $scope.atm.days_available = tmp.join();

      $scope.atm.start_time = moment($scope.start_time).utcOffset(0, true).format('HH:mm:ss');
      $scope.atm.close_time = moment($scope.close_time).utcOffset(0, true).format('HH:mm:ss');
      // console.log(moment);

      ATM.add($scope.atm)
      .then(function(response) {
        $scope.messages = {
          success: [response.data]
        };
        $location.path('/atms');
      })
      .catch(function(response) {
        $scope.messages = {
          error: Array.isArray(response.data) ? response.data : [response.data]
        };
      });
    }
  });




angular.module('MyApp')
  .controller('AtmEditCtrl', function($scope, $location,$routeParams, $window, $auth, ATM) {
    $scope.atm = {title: '', description: '', id:$routeParams.id, days_available: '' , has_buy_option : "no", has_sell_option : "no"}
        $scope.days = {
          day_1: false,
          day_2: false,
          day_3: false,
          day_4: false,
          day_5: false,
          day_6: false,
          day_0: false,
        };
    $scope.title = 'Edit ATM';
    $scope.submitForm = function(){
      if($scope.atm.title == '' || $scope.atm.description == '')
        return;


        $scope.atm.start_time = moment($scope.start_time).utcOffset(0, true).format('HH:mm:ss');
        $scope.atm.close_time = moment($scope.close_time).utcOffset(0, true).format('HH:mm:ss');

      var tmp =  [];
      for(var i  = 0 ; i <= 7 ; i++)
      {
        if( $scope.days['day_'+i])
          tmp.push(i);
      }
      $scope.atm.days_available = tmp.join();


      ATM.update($scope.atm)
      .then(function(response) {
        $scope.messages = {
          success: [response.data]
        };
        // $location.path('/atm');
      })
      .catch(function(response) {
        $scope.messages = {
          error: Array.isArray(response.data) ? response.data : [response.data]
        };
      });
    }



    $scope.start_time = new Date();
    $scope.close_time = new Date();

    $scope.fetchSingle = function(){
      ATM.listSingle($routeParams.id)
      .then(function(response) {
        // console.log(response)
          $scope.atm = response.data.atm;
          var ats = $scope.atm.days_available.split(',');
          for(var i = 0 ; i < ats.length;i++)
          {
            $scope.days['day_'+ats[i]]  =true;
            // ats
          }

          $scope.start_time = moment(response.data.atm.start_time, "HH:mm:ss").toDate();
          $scope.close_time = moment(response.data.atm.close_time, "HH:mm:ss").toDate();
          $scope.atm.has_buy_option =  response.data.atm.has_buy_option ? 'yes' : 'no';
          $scope.atm.has_sell_option =  response.data.atm.has_sell_option ? 'yes' : 'no';
      })
      .catch(function(response) {
        $scope.atm = {title: '', description: '', id:$routeParams.id };
        $scope.messages = {
          error: Array.isArray(response.data) ? response.data : [response.data]
        };
      });
    }
    $scope.fetchSingle();
  });
