angular.module('MyApp')
.controller('HomeCtrl', function($scope, NOTIFS){

  $scope.notifs = [ ];
  $scope.show = false;

  $scope.list = function(){
    $scope.show = false;
    NOTIFS.send()
    .then(function(response) {
      // console.log(response)
      $scope.show = true;
        $scope.notifs = response.data.notifs;
    })
    .catch(function(response) {
        $scope.notifs = [ ];
        $scope.show = true;
      $scope.messages = {
        error: Array.isArray(response.data) ? response.data : [response.data]
      };
    });
  }
  $scope.list();
  $scope.delete = function(id)
  {

      NOTIFS.delete({id:id})
      .then(function(response) {
        $scope.messages = {
          success: [response.data]
        };
        $scope.list();
      })
      .catch(function(response) {
        // $scope.messages = {
        //   error: Array.isArray(response.data) ? response.data : [response.data]
        // };
      });
  }



})


.controller('AtmMachineCtrl', function($scope, Contact){

  $scope.rows = [ ];
  $scope.show = false;

  $scope.list = function(){
    $scope.show = false;
    Contact.list()
    .then(function(response) {
      // console.log(response)
      $scope.show = true;
        $scope.rows = response.data.rows;
    })
    .catch(function(response) {
        $scope.rows = [ ];
        $scope.show = true;
      $scope.messages = {
        error: Array.isArray(response.data) ? response.data : [response.data]
      };
    });
  }
  $scope.list();
  $scope.delete = function(id)
  {

      NOTIFS.delete({id:id})
      .then(function(response) {
        $scope.messages = {
          success: [response.data]
        };
        $scope.list();
      })
      .catch(function(response) {
        // $scope.messages = {
        //   error: Array.isArray(response.data) ? response.data : [response.data]
        // };
      });
  }



})




  .controller('CryptCtrl', function($scope, CRYPT)
  {

    $scope.currencies = {
      GBP : 000,
      ILS : 000,
      USD : 000
    }

    $scope.fetchPRice = function(idx, id)
    {
      CRYPT.price(id)
        .then(function(response)
        {
          if (response.data && response.data.data)
          {
            $scope.currencies[id] = Math.round(response.data.data.quotes[id].price * 100) / 100;

          }
					console.log($scope.currencies)

          // $scope.blogposts = response.data.posts;
        })
        .catch(function(response)
        {
          console.log(response);
          $scope.blogposts = [];
          $scope.messages = {
            error: Array.isArray(response.data) ? response.data : [response.data]
          };
        });
    }
    // for (var i = 0; i < 3; i++)
    // {
    $scope.fetchPRice(0, "USD");
    $scope.fetchPRice(0, "GBP");
    $scope.fetchPRice(0 , "ILS");
    // }
  });
