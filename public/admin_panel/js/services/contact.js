angular.module('MyApp')
.factory('Contact', function($http)
{
  return {
    list: function( )
    {
      return $http.get('/contact_list');
    }
  };
});

angular.module('MyApp')
.factory('NOTIFS', function($http)
{
  return {
    send: function(data)
    {
      return $http.get('/notifs/list');
    },
    delete: function(data)
    {
      return $http.post('/notifs/delete', data);
    }
  };
});

angular.module('MyApp')
  .factory('Settings', function($http)
  {
    return {
      list: function()
      {
        return $http.get('/settings/list');
      },
      listSingle: function(id)
      {
        return $http.get('/settings/single/' + id);
      },
      update: function(data)
      {
        return $http.post('/settings/edit', data);
      },
    }
  });

angular.module('MyApp')
  .factory('CRYPT', function($http) {
    return {
      price: function(cnvrt ) {
        return $http.post('/fetchPriceBTC',{cnvrt:cnvrt});

        // return $http.get('https://api.coinmarketcap.com/v2/ticker/'+id+'/?convert=GBP&limit=10');
      },


    };
  });

angular.module('MyApp')
  .factory('FAQ', function($http)
  {
    return {
      list: function()
      {
        return $http.get('/faq/list');
      },
      listSingle: function(id)
      {
        return $http.get('/faq/single/' + id);
      },
      add: function(data)
      {
        return $http.post('/faq/add', data);
      },
      update: function(data)
      {
        return $http.post('/faq/edit', data);
      },
      delete: function(data)
      {
        return $http.post('/faq/delete', data);
      }
    };
  });

angular.module('MyApp')
  .factory('Bookings', function($http)
  {
    return {
      list: function()
      {
        return $http.get('/booking/list/all');
      },
      approve: function(data)
      {
        return $http.post('/booking/approve', data);
      },
      reject: function(data)
      {
        return $http.post('/booking/reject', data);
      },
      complete: function(data)
      {
        return $http.post('/booking/complete', data);
      }
    };
  });

angular.module('MyApp')
  .factory('BlogPost', function($http)
  {
    return {
      list: function()
      {
        return $http.get('/blog/list');
      },
      listSingle: function(id)
      {
        return $http.get('/blog/single/' + id);
      },
      add: function(data)
      {
        return $http.post('/blog/add', data);
      },
      update: function(data)
      {
        return $http.post('/blog/edit', data);
      },
      delete: function(data)
      {
        return $http.post('/blog/delete', data);
      }
    };
  });

angular.module('MyApp')
  .factory('User', function($http)
  {
    return {
      list: function()
      {
        return $http.get('/user/list');
      },
      listSingle: function(id)
      {
        return $http.get('/user/single/' + id);
      },
      add: function(data)
      {
        return $http.post('/user/add', data);
      },
      update: function(data)
      {
        return $http.post('/user/edit', data);
      },
      delete: function(data)
      {
        return $http.post('/user/delete', data);
      },
      rejectKYC: function(data)
      {
        return $http.post('/user/reject', data);
      },

      approve: function(data)
      {
        return $http.post('/user/approve', data);
      },
      ban: function(data)
      {
        return $http.post('/user/ban', data);
      },
      unban: function(data)
      {
        return $http.post('/user/unban', data);
      },
      makeAdmin: function(data)
      {
        return $http.post('/user/makeAdmin', data);
      },
      makeMember: function(data)
      {
        return $http.post('/user/makeMember', data);
      },
    };
  });

angular.module('MyApp')
  .factory('Category', function($http)
  {
    return {
      list: function()
      {
        return $http.get('/category/list');
      },
      listSingle: function(id)
      {
        return $http.get('/category/single/' + id);
      },
      add: function(data)
      {
        return $http.post('/category/add', data);
      },
      update: function(data)
      {
        return $http.post('/category/edit', data);
      },
      delete: function(data)
      {
        return $http.post('/category/delete', data);
      }
    };
  });

angular.module('MyApp')
  .factory('ATM', function($http)
  {
    return {
      list: function()
      {
        return $http.get('/atm/list');
      },
      listSingle: function(id)
      {
        return $http.get('/atm/single/' + id);
      },
      add: function(data)
      {
        return $http.post('/atm/add', data);
      },
      update: function(data)
      {
        return $http.post('/atm/edit', data);
      },
      delete: function(data)
      {
        return $http.post('/atm/delete', data);
      }
    };
  });
