angular.module('MyApp', ['ngRoute', 'satellizer','oitozero.ngSweetAlert', 'summernote'])
  .config(function($routeProvider, $locationProvider, $authProvider)
  {
    $locationProvider.html5Mode(true);

    $routeProvider
      .when('/',
      {
      controller: 'HomeCtrl',
        resolve:
        {
          loginRequired: loginRequired,

        },
        templateUrl: 'partials/home.html'
      })
			.when('/exchange_rates',
      {
        resolve:
        {
          loginRequired: loginRequired
        },
        templateUrl: 'partials/exchange_rates.html',
				 controller: 'CryptCtrl',
      })

      .when('/settings/',
      {
        resolve:
        {
          loginRequired: loginRequired
        },

        templateUrl: 'partials/settings/list.html',
        controller: 'SettingCtrl',

      })  .when('/settings/edit/:id',
        {
          resolve:
          {
            loginRequired: loginRequired
          },
          templateUrl: 'partials/settings/add.html',
          controller: 'SettingsEditCtrl',

        })

              .when('/atms/',
              {
                resolve:
                {
                  loginRequired: loginRequired
                },
                templateUrl: 'partials/atms/list.html',
                controller: 'AtmCtrl',

              })
                    .when('/atm_machine_requests/',
                    {
                      resolve:
                      {
                        loginRequired: loginRequired
                      },
                      templateUrl: 'partials/atm_machine_requests.html',
                      controller: 'AtmMachineCtrl',

                    })


      .when('/atms/add',
      {
        resolve:
        {
          loginRequired: loginRequired
        },
        controller: 'AtmAddCtrl',
        templateUrl: 'partials/atms/add.html'
      })

      .when('/atms/edit/:id',
      {
        resolve:
        {
          loginRequired: loginRequired
        },
        controller: 'AtmEditCtrl',
        templateUrl: 'partials/atms/add.html'
      })

      .when('/blog-posts/',
      {
        resolve:
        {
          loginRequired: loginRequired
        },
        templateUrl: 'partials/blog/list.html',
        controller: 'BlogCtrl',

      })

      .when('/blog-posts/add',
      {
        resolve:
        {
          loginRequired: loginRequired
        },
        controller: 'BlogAddCtrl',
        templateUrl: 'partials/blog/add.html'
      })

      .when('/blog-posts/edit/:id',
      {
        resolve:
        {
          loginRequired: loginRequired
        },
        controller: 'BlogEditCtrl',
        templateUrl: 'partials/blog/add.html'
      })

      .when('/users/',
      {
        resolve:
        {
          loginRequired: loginRequired
        },
        controller: 'UserCtrl',
        templateUrl: 'partials/users/list.html'
      })

      .when('/category/',
      {
        resolve:
        {
          loginRequired: loginRequired
        },
        controller: 'CategoryCtrl',
        templateUrl: 'partials/category/list.html'
      })

      .when('/category/add',
      {
        resolve:
        {
          loginRequired: loginRequired
        },
        controller: 'CategoryAddCtrl',
        templateUrl: 'partials/category/add.html'
      })

      .when('/category/edit/:id',
      {
        resolve:
        {
          loginRequired: loginRequired
        },
        controller: 'CategoryEditCtrl',
        templateUrl: 'partials/category/add.html',
      })

      .when('/faq/',
      {
        resolve:
        {
          loginRequired: loginRequired
        },
        controller: 'FaqCtrl',
        templateUrl: 'partials/faq/list.html'
      })

      .when('/faq/add',
      {
        resolve:
        {
          loginRequired: loginRequired
        },
        controller: 'FaqAddCtrl',
        templateUrl: 'partials/faq/add.html'
      })

      .when('/faq/edit/:id',
      {
        resolve:
        {
          loginRequired: loginRequired
        },
        controller: 'FaqEditCtrl',
        templateUrl: 'partials/faq/add.html',
      })

      .when('/bookings/',
      {
        resolve:
        {
          loginRequired: loginRequired
        },
        controller: 'BookingsCtrl',
        templateUrl: 'partials/bookings.html'
      })
      .when('/upload_invoices/',
      {
        resolve:
        {
          loginRequired: loginRequired
        },
        controller: 'BookingsCtrl',
        templateUrl: 'partials/upload_invoices.html'
      })



      .otherwise(
      {
        templateUrl: 'partials/404.html'
      });

    $authProvider.loginUrl = '/login';
    $authProvider.signupUrl = '/signup';

    function skipIfAuthenticated($location, $auth)
    {
      if ($auth.isAuthenticated())
      {
        $location.path('/');
      }
    }

    function loginRequired($location, $auth, $http, $window)
    {

      if (!$auth.isAuthenticated())
      {
        window.location.href = window.location.origin + "/login"
      }

      $http.get('/me').then(function(res){
        if(res.data && res.data.user)
        {

          if(res.data.user.status  == false)
          {
            $auth.logout();
            delete $window.localStorage.user;
            window.location.href =  window.location.origin ;
          }

          // $rootScope.currentUser = response.data.user;
          // $window.localStorage. = ;
          localStorage.setItem('user', JSON.stringify(res.data.user));


          if(res.data.user.role != 'admin')
          {
            window.location.href = window.location.origin + "/account"
          }

        }else{
          window.location.href = window.location.origin + "/account"
        }

      })
      .catch(function(er){
        console.log(er)
          window.location.href = window.location.origin + "/login"
      })



    }
  })
  .run(function($rootScope, $window, $location)
  {
    if ($window.localStorage.user)
    {
      $rootScope.currentUser = JSON.parse($window.localStorage.user);
    }
    $rootScope.urlhas = function(f)
    {
      return (($location['$$path']).indexOf(f) >= 0);
    }
  });
