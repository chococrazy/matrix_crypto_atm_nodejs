angular.module('MyApp', ['ngRoute', 'satellizer', 'ngMap', 'pascalprecht.translate'])
  .config(function($routeProvider, $locationProvider, $authProvider, $translateProvider) {
    $locationProvider.html5Mode(true);

    $translateProvider.translations('en', {
      OUR_PARTNER_SITES      : 'Our Partner Sites',
      PAGE_NOT_FOUND         : 'Page Not Found',
      ABOUT_US               : 'About Us',
      ABOUT_COMPANY          : 'About Company',
      WHY_CHOOSE_US          : 'Why Choose Us',
      IF_YOU_THINK           : 'If you think it\'s just you\'re looking for. Please contact us!',
      CONTACT_US             : 'Contact Us',
      OUR_BLOG               : 'Our Blog',
      POSTED_BY              : 'Posted By ',
      READ_MORE              : 'Read More',
      RECENT_POST            : 'Recent Posts',
      CATEGORIES             : 'Categories',
      BLOG                   : 'Blog',
      MY_BOOKINGS            : 'My Bookings',
      ACCOUNT                : 'Account',
      BOOK_A_VISIT           : 'Book a visit',
      MY_KYC                 : 'My KYC',
      CHANGE_PWD             : 'Change Password',
      LOGOUT                 : 'Logout',
      ID                     : 'ID',
      ATM_TITLE              : 'ATM Title',
      TNX_TYP_AMNT           : 'Transaction Type/Amount',
      ACTIONS                : 'Actions',
      STATUS                 : 'Status',
      CREATED_AT             : 'Created At',
      CANCEL_BOOKING         : 'Cancel Booking',
      ARE_YOU_SURE           : 'Are you sure ?',
      DOWNLOAD_INV           : 'Download Invoice',
      ADDRESS                : 'ADDRESS',
      PHONE                  : 'PHONE',
      EMAIL                  : 'EMAIL-ID',
      CONTACT_INFO           : 'Contact Information',
      FAQ_TITLE              : 'Frequent Asked Questions',
      CONTACT_INFO           : 'Contact Info',
      OPENING_HOURS          : 'Opening Hours',
      RESET_PWD              : 'Reset Password?',
      FORGOT_PWD             : 'Forgot Your Password?',
      LOCATION               : 'Location',
      PROFILE                : 'Profile',
      PWD                    : 'Password',
      HOME                   : 'Home',
      LOCATIONS              : 'Locations',
      QA                     : 'Q&A',
      BLOG                   : 'Blog',
      CONTACT                : 'Contact',
      LOGIN                  : 'Log In',
      REGISTER               : 'Register',
      DASHBOARD              : 'Dashboard',
      ADMIN                  : 'Admin',
      FILTER_CITY            : 'Filter City',
      FILTER_PROVIDER        : 'Filter Provider',
      FILTER                 : 'Filter',
      SWITCH_TO              : 'עבור לעברית',
      GET_IN_TOUCH           : 'Get In Touch',
      WANT_TO_BUY_CRYPT      : 'Want to buy cryptographic coins?',
      SUBSCR_TXT             : 'Subscribe to the system and set up a guidance meeting and consultation with our staff! ',
      SUBMIT                 : 'Submit',
      BUY_ATM_MACHINE        : 'Buy ATM Machine',
      ENTER_FIRST_NAME       : 'Enter First Name',
      ENTER_NAME             : 'Enter Name',
      ENTER_LAST_NAME        : 'Enter Last Name',
      ENTER_EMAIL            : 'Enter Email',
      ENTER_COMPANY          : 'Enter Company',
      SUBMIT_YOUR_QUERY      : 'Submit Your Query',
      MESSAGE                : 'Message',
      FOR_TRADERS            : 'For Traders',
      GO_TO_DASH             : 'Go to your Dashboard',
      REGISTER_NOW           : 'Register Now',
      ENTER_PHONE            : 'Enter Phone No.',
      ENTER_SUBJECT          : 'Enter Subject',
      ENTER_EMAIL_FOR_PAS_RE : "Enter email address for reset password.",
      SELL                   : 'Sell',
      BUY                    : 'Buy',
      BUY_SELL               : 'Buy & Sell',
      NO_RESULTS             : 'There are no search results for this search.',
      GET_DIRECTIONS         : 'Get Direction',
      SCHEDULE_VISIT         : 'Schedule Visit',
      DESC                   : 'Description',
      PHONE_NUMBER           : 'Phone Number',
      TIMINGS                : 'Timings',
      DAYS                   : 'Days',
      AVAILABILITY           : 'Availability',
      COMMISSION_BUY         : 'Commission - buy',
      COMMISSION_SELL        : 'Commission - sell',
      SEARCH_PLDR            : 'Search...',
      CLOSE                  : 'Close',
      SCHEDULE               : 'Schedule',
      HOW_MUCH               : 'How much do you want to',
      TX_TYP                 : 'Transaction Type',
      SELECT                 : 'Select',
      SELECT_VISIT_DATE      : 'Select Visit Date',
      SELECT_VISIT_TIME      : 'Select Visit Time',
      NOTE                   : 'Note',
      SHOULD_BE_BETWEEN      : 'Should be Between',
      TO                     : 'to',
      ALERT                  : 'Alert',
      KYC_NOT_APPR           : 'Your KYC is yet not approved by admin. You will be able to schedule a visit only upon KYC approval.',
      UPDATE_KYC             : 'Update Your KYC',
      KYC_NOT_UPLD           : 'You have still not updated your kyc details, please visit your dashboard to update your kyc.',
      ENTER_PASSWORD         : "Enter password",
      CONFIRM_PASSWORD       : 'Confirm Password',
      ADDRESS                : 'Address',
      CITY                   : 'City',
      ZIPCODE                : 'ZipCode',
      COUNTRY                : 'Country',
      I_AGREE                : 'I agree with ',
      TERMS_OF_SERVICE       : 'Terms of Services',
      AUTH_DETAILS           : 'Authentication Details',
      FULL_NAME              : 'Full Name',
      IN_CASE_NO_MAIL        : 'In-case, you didnt receive any mail',
      CLICK_HERE             : 'click here',
      TO_RESPOND             : 'to resend',
      REGISTER_ACCOUNT       : 'Register Account',
      CREATE_FREE_ACCOUNT    : 'Create your Free account',
      RESETING_PEWD          : 'Resetting your Password?',
      ENTER_NEW_PWD          : 'Enter the new password.',
      CONTACT_DETAILS        : 'Contact Details',
      KYC_YET                : 'Your KYC is yet to be approved by admin.',
      KYC_REJ                : 'Your KYC has been rejected by admin. Please re-upload',
      KYC_APR                : 'Your KYC has been approved by admin.',
      SELECT_ID_PROOF        : 'Select ID Proof type',
      SELECT                 : 'Select',
      PASSPORT_NM            : 'Passport No',
      DRIVER_LC              : 'Driver License',
      NAT_ID                 : 'Nationality Id',
      IDENTITY               : 'Identity Number',
      PREV_KYC_REC           : 'Previously Uploaded KYC Records',
      IDENTITY_PRF           : 'Identity Proof',
      IDENTITY_TYP           : 'Identity Proof Type',
      UPDATE_PROFILE         : 'Update Profile',
      DOB                    : 'Date of Birth',
      MALE                   : 'Male',
      FEMALE                 : 'Female',
      WELCOME                : 'Welcome',
      SIGN_IN_TO             : 'Sign in to your account',
      DOWNLOAD               : 'Download',



    });

    $translateProvider.translations('he', {
      OUR_PARTNER_SITES      : 'אתרי השותפים שלנו',
      PAGE_NOT_FOUND         : 'הדף לא נמצא',
      ABOUT_US               : 'אודותינו',
      ABOUT_COMPANY          : 'אודות החברה',
      WHY_CHOOSE_US          : 'למה לבחור בנו',
      IF_YOU_THINK           : 'אם אתה חושב שזה רק אתה מחפש. אנא צרו קשר!',
      CONTACT_US             : 'צור קשר',
      OUR_BLOG               : 'הבלוג שלנו',
      POSTED_BY              : 'פורסם על ידי',
      READ_MORE              : 'קרא עוד',
      RECENT_POST            : 'הודעות אחרונות',
      CATEGORIES             : 'קטגוריות',
      BLOG                   : 'בלוג',
      MY_BOOKINGS            : 'ההזמנות שלי',
      ACCOUNT                : 'חשבון',
      BOOK_A_VISIT           : 'להזמין ביקור',
      MY_KYC                 : 'KYC שלי',
      CHANGE_PWD             : 'שינוי סיסמה',
      LOGOUT                 : 'Logout',
      ID                     : 'מזהה',
      ATM_TITLE              : 'כותרת כספומט',
      TNX_TYP_AMNT           : 'סוג עסקה / סכום',
      ACTIONS                : 'פעולות',
      STATUS                 : 'מצב',
      CREATED_AT             : 'נוצר ב',
      CANCEL_BOOKING         : 'ביטול הזמנה',
      ARE_YOU_SURE           : 'ללא שם: האם אתה בטוח?',
      DOWNLOAD_INV           : 'הורד את החשבונית',
      ADDRESS                : 'כתובת',
      PHONE                  : 'טלפון',
      EMAIL                  : 'מזהה אימייל',
      CONTACT_INFO           : 'מידע ליצירת קשר',
      FAQ_TITLE              : 'שאלות נפוצות',
      CONTACT_INFO           : 'מידע ליצירת קשר',
      OPENING_HOURS          : 'שעות פתיחה',
      RESET_PWD              : 'איפוס סיסמה?',
      FORGOT_PWD             : 'שכחת את הסיסמה שלך?',
      LOCATION               : 'מיקום',
      PROFILE                : 'פרופיל',
      PWD                    : 'סיסמא',
      HOME                   : 'בית',
      LOCATIONS              : 'מיקומים',
      QA                     : 'שאלות ותשובות',
      BLOG                   : 'בלוג',
      CONTACT                : 'יצירת קשר',
      LOGIN                  : 'היכנס',
      REGISTER               : 'לרשום',
      DASHBOARD              : 'לוח מחוונים',
      ADMIN                  : 'מנהל',
      SWITCH_TO              : 'Switch to English',
      GET_IN_TOUCH           : 'קבל קשר',
      WANT_TO_BUY_CRYPT      : 'רוצה לקנות מטבעות קריפטוגרפיים?',
      SUBSCR_TXT             : 'הירשם למערכת וקבע פגישה והדרכה עם הצוות שלנו!',
      SUBMIT                 : 'להגיש',
      BUY_ATM_MACHINE        : 'קנה כספומט',
      ENTER_FIRST_NAME       : 'הזן שם פרטי',
      ENTER_NAME             : 'הכנס שם',
      ENTER_LAST_NAME        : 'הזן שם משפחה',
      ENTER_EMAIL            : 'הזן דוא"ל',
      ENTER_COMPANY          : 'הזן את החברה',
      SUBMIT_YOUR_QUERY      : 'שלח את השאילתה שלך',
      MESSAGE                : 'הוֹדָעָה',
      FOR_TRADERS            : 'עבור סוחרים',
      GO_TO_DASH             : 'עבור אל מרכז השליטה שלך',
      REGISTER_NOW           : 'הירשם עכשיו',
      ENTER_PHONE            : 'הזן מספר טלפון',
      ENTER_SUBJECT          : 'הזן נושא',
      ENTER_EMAIL_FOR_PAS_RE : "הזן כתובת דוא\"ל לאיפוס סיסמה.",
      FILTER_CITY            : 'סנן לפי מיקום / עיר / מדינה',
      FILTER_PROVIDER        : 'מסנן לפי שירות מסופק',
      FILTER                 : "לְסַנֵן",
      SELL                   : 'מכירה',
      BUY                    : 'לִקְנוֹת',
      BUY_SELL               : 'לקנות למכור',
      NO_RESULTS             : 'אין תוצאות חיפוש עבור חיפוש זה.',
      GET_DIRECTIONS         : 'קבל כיוון',
      SCHEDULE_VISIT         : 'ביקור בתוספת',
      DESC                   : 'תיאור',
      PHONE_NUMBER           : 'מספר טלפון',
      TIMINGS                : 'עיתוי',
      DAYS                   : 'ימים',
      AVAILABILITY           : 'זמינות',
      COMMISSION_BUY         : '- קניה',
      COMMISSION_SELL        : 'עמלה - מכירה',
      SEARCH_PLDR            : 'לחפש...',
      CLOSE                  : 'לִסְגוֹר',
      SCHEDULE               : 'לוח זמנים',
      HOW_MUCH               : 'כמה אתה רוצה',
      TX_TYP                 : 'סוג עסקה',
      SELECT                 : 'בחר',
      SELECT_VISIT_DATE      : 'בחר באפשרות תאריך ביקור',
      SELECT_VISIT_TIME      : 'בחר זמן ביקור',
      NOTE                   : 'הערה',
      SHOULD_BE_BETWEEN      : 'צריך להיות בין',
      TO                     : 'ל',
      ALERT                  : 'עֵרָנִי',
      KYC_NOT_APPR           : 'KYC שלך עדיין לא אושרה על ידי מנהל המערכת. תוכל לתזמן ביקור רק על אישור KYC.',
      UPDATE_KYC             : 'עדכן את KYC',
      KYC_NOT_UPLD           : 'עדיין לא עדכנת את פרטי ה- Kyc שלך, בקר במרכז השליטה כדי לעדכן את ה- kyc שלך.',
      ENTER_PASSWORD         : "הזן את הסיסמה",
      CONFIRM_PASSWORD       : 'אישור Password',
      ADDRESS                : 'כתובת',
      CITY                   : 'עִיר',
      ZIPCODE                : 'מיקוד',
      COUNTRY                : 'מדינה',
      I_AGREE                : 'אני מסכים עם',
      TERMS_OF_SERVICE       : 'תנאי השירות',
      AUTH_DETAILS           : 'פרטי אימות',
      FULL_NAME              : 'שם מלא',
      IN_CASE_NO_MAIL        : 'במקרה, אתה did not לקבל כל דואר',
      CLICK_HERE             : 'לחץ כאן',
      TO_RESPOND             : 'לשלוח שוב',
      REGISTER_ACCOUNT       : 'הרשמה לחשבון',
      CREATE_FREE_ACCOUNT    : 'צור את החשבון שלך בחינם',
      RESETING_PEWD          : 'מאפס את הסיסמה שלך?',
      ENTER_NEW_PWD          : 'הזן את הסיסמה החדשה.',
      CONTACT_DETAILS        : 'פרטי קשרs',
      KYC_YET                : 'KYC שלך עדיין לא אושרה על ידי מנהל המערכת.',
      KYC_REJ                : 'KYC שלך נדחתה על ידי מנהל המערכת. העלה מחדש',
      KYC_APR                : 'KYC שלך אושרה על ידי מנהל המערכת.',
      SELECT_ID_PROOF        : 'בחר סוג הוכחה מזהה',
      SELECT                 : 'בחר',
      PASSPORT_NM            : 'מספר דרכון',
      DRIVER_LC              : 'רשיון נהיגה',
      NAT_ID                 : 'תעודת זהות',
      IDENTITY               : 'מספר זהות',
      PREV_KYC_REC           : 'בעבר הועלה KYC Records',
      IDENTITY_PRF           : 'הוכחת זהות',
      IDENTITY_TYP           : 'סוג הוכחת זהות',
      UPDATE_PROFILE         : 'עדכן פרופיל',
      DOB                    : 'תאריך לידה',
      MALE                   : 'זָכָר',
      FEMALE                 : 'נְקֵבָה',
      WELCOME                : 'ברוך הבא',
      SIGN_IN_TO             : 'תתחבר לחשבון שלך',
      DOWNLOAD               : 'הורד',


    });

    $translateProvider.preferredLanguage('en');



    $routeProvider
      .when('/', {
        controller: 'HomeCtrl',
        templateUrl: 'partials/home.html'
      })
      .when('/contact', {
        templateUrl: 'partials/contact.html',
        controller: 'ContactCtrl'
      })
      .when('/buy-atm-machines', {
        templateUrl: 'partials/buyatmmachines.html',
        controller: 'ContactCtrl'
      })

      .when('/faq', {
        templateUrl: 'partials/faq.html',
        controller: 'FAQCtrl'
      })
      .when('/about', {
        templateUrl: 'partials/about.html',
        // controller: 'FAQCtrl'
      })
			 .when('/locations', {
        templateUrl: 'partials/location.html',
        controller: 'LocationCtrl'
      })
			.when('/blog', {
        templateUrl: 'partials/blog.html',
        controller: 'BlogCtrl'
      })
			.when('/blog/:id/:slug', {
        templateUrl: 'partials/blogSingle.html',
        controller: 'BlogSingleCtrl'
      })
      .when('/login', {
        templateUrl: 'partials/login.html',
        controller: 'LoginCtrl',
        resolve: { skipIfAuthenticated: skipIfAuthenticated }
      })
      .when('/login/:successVerify', {
        templateUrl: 'partials/login.html',
        controller: 'LoginCtrl',
        resolve: { skipIfAuthenticated: skipIfAuthenticated }
      })
      .when('/signup', {
        templateUrl: 'partials/signup.html',
        controller: 'SignupCtrl',
        resolve: { skipIfAuthenticated: skipIfAuthenticated }
      })
      .when('/account', {
        templateUrl: 'partials/profile.html',
        controller: 'ProfileCtrl',
        resolve: { loginRequired: loginRequired }
      })
      .when('/account/kyc', {
        templateUrl: 'partials/profileKYC.html',
        controller: 'ProfileCtrl',
        resolve: { loginRequired: loginRequired }
      })
      .when('/account/change-password', {
        templateUrl: 'partials/profilePassword.html',
        controller: 'ProfileCtrl',
        resolve: { loginRequired: loginRequired }
      })
      .when('/bookings', {
        templateUrl: 'partials/bookings.html',
        controller: 'BookingsCtrl',
        resolve: { loginRequired: loginRequired }
      })
      .when('/forgot', {
        templateUrl: 'partials/forgot.html',
        controller: 'ForgotCtrl',
        resolve: { skipIfAuthenticated: skipIfAuthenticated }
      })
      .when('/reset/:token', {
        templateUrl: 'partials/reset.html',
        controller: 'ResetCtrl',
        resolve: { skipIfAuthenticated: skipIfAuthenticated }
      })
      .otherwise({
        templateUrl: 'partials/404.html'
      });

    $authProvider.loginUrl = '/login';
    $authProvider.signupUrl = '/signup';

    function skipIfAuthenticated($location, $auth) {
      if ($auth.isAuthenticated()) {
        $location.path('/account');
      }
    }

    function loginRequired($location, $auth) {
      if (!$auth.isAuthenticated()) {
        $location.path('/login');
      }
    }
  })
  .run(function($rootScope, $window, $http, $auth,  $translate) {
    $rootScope.lang = 'en';

    $rootScope.currentLang = function()
    {
      return( $translate.use() );
    }


    $http.get('/settings/list/default').then(function(response){
        if(response.data.ok)
        {
          $rootScope.settings = $rootScope.formatSettings(response.data.settings);
          // console.log($rootScope.settings);
        }
    });

    if($window.localStorage.user)
    {
      $http.get('/me').then(function(res){
        // alert()
        if(res.data && res.data.user)
        {


          if(res.data.user.status  == false)
          {
            $auth.logout();
            delete $window.localStorage.user;
            $location.path('/');
          }

          // $rootScope.currentUser = response.data.user;
          // $window.localStorage. = ;
          localStorage.setItem('user', JSON.stringify(res.data.user));


          if(res.data.user.role != 'admin')
          {
            // window.location.href = window.location.origin + "/account"
          }

        }else{
          $auth.logout();
          delete $window.localStorage.user;
          $location.path('/');
        }

      })
      .catch(function(er){
        $auth.logout();
        delete $window.localStorage.user;
        window.location.href = window.location.origin + "/login"
      })
    }

    $rootScope.formatSettings = function(data){
      var set = {};
      for(var i = 0 ;i < data.length; i++ )
      {
        tmp  = data[i].key;
        set[tmp] = data[i].content;
        set[tmp+'_he'] = data[i].content_hebrew;
      }

      console.log(set)
      return set;

    }

    $rootScope.$on("$routeChangeSuccess", function (event, currentRoute, previousRoute) {
      setTimeout(function(){
        if($('#no_KYC').length)
          $('#no_KYC').modal('hide');
        $('.modal-backdrop').remove();
        $('body').removeClass('modal-open');
        $('.navbar-collapse').collapse('hide');

        window.scrollTo(0, 0);

      },200);

    });
    if ($window.localStorage.user) {
      $rootScope.currentUser = JSON.parse($window.localStorage.user);

      $rootScope.globalSettigs = { facebook : 'http://facebook.com', twitter : 'https://twitter.com', instagram : 'https://instagram.com' }
    }
  });
