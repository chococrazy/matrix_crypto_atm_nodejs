angular.module('MyApp')
  .factory('Contact', function($http) {
    return {
      send: function(data) {
        return $http.post('/contact', data);
      },
      sendATM : function(data)
      {
        return $http.post('/contactATM', data)
      }
    };
  });

angular.module('MyApp')
  .factory('FAQ', function($http) {
    return {
      list: function( ) {
        return $http.get('/faq/list');
      },
      listSingle : function(id){
        return $http.get('/faq/single/'+id);
      },

    };
  });


  angular.module('MyApp')
    .factory('ATM', function($http) {
      return {
        list: function( ) {
          return $http.get('/atm/list');
        },
        listSearch : function(filter){
          return $http.post('/atm/list/search', filter);
        },
        listSingle : function(id){
          return $http.get('/atm/single/'+id);
        },

      };
    });

angular.module('MyApp')
  .factory('Booking', function($http) {
    return {
      list: function( ) {
        return $http.get('/booking/list');
      },
      new : function(booking){
        return $http.post('/booking/new', booking);
      },
      cancel : function(booking){
        return $http.post('/booking/cancel', booking);
      }


    };
  });

angular.module('MyApp')
.factory('BlogPost', function($http) {
  return {
    list: function( ) {
      return $http.get('/blog/list');
    },
    listSingle : function(id){
      return $http.get('/blog/single/'+id);
    },

  };
});

angular.module('MyApp')
  .factory('CRYPT', function($http) {
    return {
      price: function(id ) {
        return $http.post('/fetchPrice',{id:id});

        // return $http.get('https://api.coinmarketcap.com/v2/ticker/'+id+'/?convert=GBP&limit=10');
      },


    };
  });
