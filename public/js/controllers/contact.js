angular.module('MyApp').filter("trust", ['$sce', function($sce) {
  return function(htmlCode){
    return $sce.trustAsHtml(htmlCode);
  }
}]);


angular.module('MyApp')
.controller('ContactCtrl', function($scope, Contact)
{

  $scope.sendContactFormATM = function()
  {
      Contact.sendATM($scope.contactATM)
        .then(function(response)
        {
          $scope.messages = {
            success: [response.data]
          };
          $scope.contactATM = {};
        })
        .catch(function(response)
        {
          $scope.messages = {
            error: Array.isArray(response.data) ? response.data : [response.data]
          };
        });
  }

  $scope.sendContactForm = function()
  {
    Contact.send($scope.contact)
      .then(function(response)
      {
        $scope.messages = {
          success: [response.data]
        };
        $scope.contact = {};
      })
      .catch(function(response)
      {
        $scope.messages = {
          error: Array.isArray(response.data) ? response.data : [response.data]
        };
      });
  };
});


angular.module('MyApp')
.controller('BookingsCtrl', function($scope, Booking)
{



  $scope.ppuur  = function(a)
  {
    var separator = '...';
    var separatorlength = separator.length ;
    var maxlength = 10 - separatorlength;


    var start = maxlength / 2 ;
    var trunc =  a.length - maxlength;
    var b = a.substring(start, trunc);
    return a.replace(b, separator);


  }




      $scope.pur = function(r)
      {
        return JSON.parse(r);
      }

      $scope.reject = function(booking)
      {
        booking.confrm_del = true;
      }
      $scope.rejectConfrm = function(id, booking)
      {

          Booking.cancel(
            {
              id: id
            })
            .then(function(response)
            {
              booking.status = 'Cancelled'
              $scope.messages = {
                success: [response.data]
              };
              setTimeout(function(){ $scope.bookingsFetch(); }, 200);
            })
            .catch(function(response)
            {
              $scope.messages = {
                error: Array.isArray(response.data) ? response.data : [response.data]
              };
              setTimeout(function(){ $scope.bookingsFetch(); }, 200);
            });

      }




      $scope.goodStatus = function(s){
        switch (s) {
          case 'pending_approval':
            return 'Pending Approval';
          case 'approved':
            return 'Approved';
          case 'rejected':
            return 'Rejected';

            break;
          default:
            return s;

        }

      }
      $scope.bookings =  [];
  $scope.bookingsFetch = function()
  {
    Booking.list()
      .then(function(response)
      {
        $scope.bookings = response.data.bookings
      })
      .catch(function(response)
      {
        $scope.bookings = [];
      });
  };
  $scope.bookingsFetch();
});

angular.module('MyApp')
.controller('FAQCtrl', function($scope, FAQ)
{

    $scope.faqs = [];

    $scope.selectedCat = '';
    $scope.cats = [];
    $scope.cats_he = [];
    $scope.processTogetCats = function(){
      for(var i = 0 ; i < $scope.faqs.length; i++)
      {
        if ( $scope.cats.indexOf( $scope.faqs[i].category) < 0 )
        {
          // alert( $scope.faqs[i].category);
          $scope.cats.push( $scope.faqs[i].category );
          $scope.cats_he.push( $scope.faqs[i].category_he ? $scope.faqs[i].category_he : $scope.faqs[i].category );
        }
      }
    }

    $scope.list = function()
    {
      FAQ.list()
        .then(function(response)
        {
          $scope.faqs = response.data.faqs;
          $scope.processTogetCats();
        })
        .catch(function(response)
        {
          $scope.faqs = [];
          $scope.messages = {
            error: Array.isArray(response.data) ? response.data : [response.data]
          };
        });
    }

    $scope.list();

  });

angular.module('MyApp')
.controller('LocationCtrl', function($scope, $rootScope, $location, $window, ATM, Booking, NgMap)
{

  $scope.linkBuildDirection = function(lat, long){
      return 'https://maps.google.com/maps?daddr=('+lat+','+long+')';
  }

    $scope.googleMapsUrl="";

    $scope.profile = $rootScope.currentUser;

    $scope.atms = [];
    $scope.search = {
      text: '',
      location: '',
      service: 'both'

    }

    $scope.doBounds = function(){
      if(!google) return;
      var bounds = new google.maps.LatLngBounds();
      for (var i=0; i<$scope.atms.length; i++) {
        var latlng = new google.maps.LatLng($scope.atms[i].lat, $scope.atms[i].long);
        bounds.extend(latlng);
      }
      NgMap.getMap().then(function(map) {
        map.setCenter(bounds.getCenter());
        map.fitBounds(bounds);
      });
    }

    $scope.list = function()
    {
      ATM.listSearch($scope.search)
        .then(function(response)
        {
          $scope.atms = response.data.atms;
          $scope.doBounds();

          if(response.data.user)
          {
            $scope.profile            = response.data.user;
            $rootScope.currentUser    = response.data.user;
            $window.localStorage.user = JSON.stringify(response.data.user);
          }
        })
        .catch(function(response)
        {
          $scope.atms = [];

          $scope.doBounds();

          $scope.messages = {
            error: Array.isArray(response.data) ? response.data : [response.data]
          };
        });
    }
    $scope.list();

    $scope.dayOf = function(a)
    {
      switch (a) {
        case 0:
          return 'Sunday';
        case 1:
          return 'Monday';
        case 2:
          return 'Tuesday';
        case 3:
          return 'Wednesday';
        case 4:
          return 'Thursday';
        case 5:
          return 'Friday';
        case 6:
          return 'Saturday';

          break;
        default:

      }
    }

    $scope.dayOfShort = function(a)
    {
      switch (a) {
        case 0:
          return 'Sun';
        case 1:
          return 'Mon';
        case 2:
          return 'Tues';
        case 3:
          return 'Wed';
        case 4:
          return 'Thurs';
        case 5:
          return 'Fri';
        case 6:
          return 'Sat';
        break;
          default:

      }
    }

    $scope.availablabilityCheck = function(enbl)
    {
      // console.log(enbl)
			if(!enbl)
				return;
      enbl = enbl.split(',');
      var dsb = [];
      for(var i = 0 ; i < 7;i++)
      {
        if( enbl.indexOf(""+i+"") < 0){

        }else{
          dsb.push( $scope.dayOf(i));
        }

      }
      return dsb.join(', ');
    }


    $scope.availablabilityCheck2 = function(enbl)
    {
			if(!enbl)
				return;
      enbl = enbl.split(',');
      var dsb = [];
      for(var i = 0 ; i < 7;i++)
      {
        if( enbl.indexOf(""+i+"") < 0){

        }else{
          dsb.push( $scope.dayOfShort(i));
        }

      }
      return dsb.join(', ');
    }

    $scope.shortOf  = function(text)
    {
      if(text.length  > 50)
        return text.substr(0, 50)+'...';
      else {
        return text;
      }
    }
		$scope.selected_atm = {}
    $scope.doFormat = function(time)
    {
      return moment(time, 'HH:mm:ss').format('hh:mm a');
    }

    $scope.schedule = function(atm)
    {
      if(!$scope.profile)
      {
        $location.path('/login');
      }

      if (!$scope.profile.kyc_approved)
      {
        if (!$scope.profile.kyc_done)
        {
          $('#no_KYC').modal()
          return;
        }

        $('#KYC_PENDING').modal();
        // alert();
        return;
      }
      $scope.selected_atm = atm;
      $scope.booking = {
        listing_id: atm.id,
        user_id: $scope.profile.id,
        time: '',
        transaction_type: '',
        date: ''
      };
      // $scope.showPopup();
      $('#myModal').modal()

      var enbl = $scope.selected_atm.days_available;
      enbl = enbl.split(',');
      var dsb = [];
      for(var i = 0 ; i < 7;i++)
      {
        if( enbl.indexOf(""+i+"") < 0)
          dsb.push(i);

      }
      $(function () {
                $('#datetimepicker1').datepicker({
                  daysOfWeekDisabled: dsb,
                  startDate: new Date(),
                  autoclose: true,
                  format:  'M dd, yyyy'


                });
            });

    }

     $scope.booking_done = false;

    $scope.finalizeSchedule = function(form)
    {

      Booking.new($scope.booking)
        .then(function(response)
        {
          $scope.messages = {
            success: [response.data]
          };
          $scope.booking_done =  true;
          // form.$setPristine();
          $scope.bookingfrm.$setPristine();
       $scope.bookingfrm.$setUntouched();
          $scope.booking = {};
        })
        .catch(function(response)
        {
          $scope.bookingfrm.$setPristine();
       $scope.bookingfrm.$setUntouched();
          // $scope.atms = [];
          $scope.booking = {};
          $scope.messages = {
            error: Array.isArray(response.data) ? response.data : [response.data]
          };
        });

    }

  });

angular.module('MyApp')
.factory('Category', function($http)
{
  return {
    list: function()
    {
      return $http.get('/category/list');
    }

  };
});

angular.module('MyApp')
.controller('BlogCtrl', function($scope, BlogPost, Category, $sce)
{

  $scope.smallerContent = function(text){

   text = $("<div>"+text+"</div>").text();

  	if(text.length  > 200)
          return text.substr(0, 200)+'...';
        else {
          return text;
        }

  }
  $scope.blogposts = [];
  $scope.categories = [];
  $scope.listCat = function()
  {
  Category.list()
    .then(function(response)
    {
      $scope.categories = response.data.categories;
    })
    .catch(function(response)
    {
      $scope.categories = [];
      $scope.messages = {
        error: Array.isArray(response.data) ? response.data : [response.data]
      };
    });
  }
  $scope.listCat();

  $scope.list = function()
  {
  BlogPost.list()
  .then(function(response)
  {
    $scope.blogposts = response.data.posts;
  })
  .catch(function(response)
  {
    $scope.blogposts = [];
    $scope.messages = {
      error: Array.isArray(response.data) ? response.data : [response.data]
    };
  });
}

$scope.list();

});

angular.module('MyApp')
.controller('HomeCtrl', function($scope,$auth, CRYPT)
{


  $scope.isAuthenticated = function() {
    return $auth.isAuthenticated();
  };


$scope.currencies = [

{
  id: 1,
  price: 000
},
{
  id: 1027,
  price: 000
},
{
  id: 2,
  price: 000
},
{
  id: 131,
  price: 000
},
{
  id: 1437,
  price: 000
},
{
  id: 1831,
  price: 000
},

]

$scope.fetchPRice = function(idx, id)
{
CRYPT.price(id)
  .then(function(response)
  {
    if (response.data && response.data.data)
    {
      $scope.currencies[idx].price = Math.round(response.data.data.quotes.USD.price * 100) / 100;

    }

    // $scope.blogposts = response.data.posts;
  })
  .catch(function(response)
  {
    $scope.blogposts = [];
    $scope.messages = {
      error: Array.isArray(response.data) ? response.data : [response.data]
    };
  });
}
for (var i = 0; i < $scope.currencies.length; i++)
{
$scope.fetchPRice(i, $scope.currencies[i].id);
}
});
















angular.module('MyApp')
  .controller('BlogSingleCtrl', function($scope, $location,$routeParams, $window, $auth,$http, BlogPost, Category) {
    $scope.blogpost = {title: '', content: '', id:$routeParams.id }
    $scope.title = 'Edit BlogPost';

	$scope.is_new = false;
    $scope.categories = [ ];


$scope.smallerContent = function(text){
	if(text.length  > 200)
        return text.substr(0, 200)+'...';
      else {
        return text;
      }

}
$scope.blogposts = [];
$scope.categories = [];
$scope.listCat = function()
{
Category.list()
  .then(function(response)
  {
    $scope.categories = response.data.categories;
  })
  .catch(function(response)
  {
    $scope.categories = [];
    $scope.messages = {
      error: Array.isArray(response.data) ? response.data : [response.data]
    };
  });
}
$scope.listCat();

$scope.list = function()
{
BlogPost.list()
  .then(function(response)
  {
    $scope.blogposts = response.data.posts;
  })
  .catch(function(response)
  {
    $scope.blogposts = [];
    $scope.messages = {
      error: Array.isArray(response.data) ? response.data : [response.data]
    };
  });
}

$scope.list();





    $scope.fetchSingle = function(){
      BlogPost.listSingle($routeParams.id)
      .then(function(response) {
        console.log(response)
          $scope.blogpost = response.data.blogpost;
      })
      .catch(function(response) {
        $scope.blogpost = {title: '', content: '', id:$routeParams.id };
        $scope.messages = {
          error: Array.isArray(response.data) ? response.data : [response.data]
        };
      });
    }
    $scope.fetchSingle();






  });
