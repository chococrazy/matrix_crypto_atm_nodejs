angular.module('MyApp')
  .controller('ProfileCtrl' , function($scope , $rootScope , $location , $window , $http , $auth , Account) {
    $scope.profile = $rootScope.currentUser;
    $scope.profile.dob = new Date($scope.profile.dob);
    console.log($scope.profile);


    $scope.isActive = function (viewLocation) {
      return viewLocation === $location.path();
    };
    $scope.upload_started = {
      path_1 : false,
      //path_2 : false
    }
    $scope.files  = { };





    $scope.uploadedFile = function(element, k)
    {

      // $scope.upload_started['path_'+k] = true;
      $scope.$apply(function($scope)
      {
        $scope.files['path_'+k] = element.files;
      });
      // $scope.addFile();
    }


    $scope.addFile = function()
    {
      $scope.upload_started = {
        path_1 : true,
        //path_2 : true
      }
      $scope.uploadfile($scope.files);
    }


        $scope.uploadfile = function(files, success, error)
        {

          var fd = new FormData();

          var url = '/profile/kyc';

          angular.forEach(files.path_1, function(file)
          {
            fd.append('file_1', file);
          });
        //  angular.forEach(files.path_2, function(file)
        //  {
        //    fd.append('file_2', file);
        //  });

          //sample data
          var data = {
            uid        : $scope.profile.id,
            kyc_1_text : $scope.profile.kyc_1_text,
            kyc_1_type : $scope.profile.kyc_1_type
          };

          fd.append("data", JSON.stringify(data));

          $http.post(url, fd,
            {
              withCredentials: false,
              headers:
              {
                'Content-Type': undefined
              },
              transformRequest: angular.identity
            })
            .success(function(data)
            {
              console.log(data);
              if(data.user){
                $rootScope.currentUser = data.user;
      					$scope.profile = data.user;
                $scope.profile.dob = new Date($scope.profile.dob);
                $window.localStorage.user = JSON.stringify(data.user);
                console.log($scope.profile);
              }
              $scope.messages = {
                success: [data]
              };

                $scope.upload_started = {
                  path_1 : false,
            //      path_2 : false
                }
              // setTimeout(function(){ $scope.list(); }, 200);
            })
            .error(function(data)
            {
              // console.log(data);
              $scope.messages = {
                error: [data]
              };

                $scope.upload_started = {
                  path_1 : false,
                  //path_2 : false
                }
            });
        };






    $scope.updateProfile = function() {
      Account.updateProfile($scope.profile)
        .then(function(response) {
          $rootScope.currentUser = response.data.user;
					$scope.profile = response.data.user;
          $scope.profile.dob = new Date($scope.profile.dob);
          $window.localStorage.user = JSON.stringify(response.data.user);
          $scope.messages = {
            success: [response.data]
          };
        })
        .catch(function(response) {
          $scope.messages = {
            error: Array.isArray(response.data) ? response.data : [response.data]
          };
        });
    };




    $scope.changePassword = function() {
      Account.changePassword($scope.profile)
        .then(function(response) {
          $scope.messages = {
            success: [response.data]
          };
        })
        .catch(function(response) {
          $scope.messages = {
            error: Array.isArray(response.data) ? response.data : [response.data]
          };
        });
    };
    //
    // $scope.link = function(provider) {
    //   $auth.link(provider)
    //     .then(function(response) {
    //       $scope.messages = {
    //         success: [response.data]
    //       };
    //     })
    //     .catch(function(response) {
    //       $window.scrollTo(0, 0);
    //       $scope.messages = {
    //         error: [response.data]
    //       };
    //     });
    // };
    // $scope.unlink = function(provider) {
    //   $auth.unlink(provider)
    //     .then(function() {
    //       $scope.messages = {
    //         success: [response.data]
    //       };
    //     })
    //     .catch(function(response) {
    //       $scope.messages = {
    //         error: [response.data]
    //       };
    //     });
    // };
    //
    // $scope.deleteAccount = function() {
    //   Account.deleteAccount()
    //     .then(function() {
    //       $auth.logout();
    //       delete $window.localStorage.user;
    //       $location.path('/');
    //     })
    //     .catch(function(response) {
    //       $scope.messages = {
    //         error: [response.data]
    //       };
    //     });
    // };
  });
