angular.module('MyApp')
  .controller('HeaderCtrl', function($scope, $rootScope, $location, $window, $auth, $translate) {
    $scope.isActive = function (viewLocation) {
      return viewLocation === $location.path();
    };
    $scope.changeLang = function(langKey){
      $translate.use(langKey);
    }
    $scope.currentLang = function()
    {
      return( $translate.use() );
    }

    $scope.isAuthenticated = function() {
      return $auth.isAuthenticated();
    };
    $scope.isSuper = function(){
      return ($rootScope.currentUser.role == 'admin')
    }

    $scope.logout = function() {
      $auth.logout();
      delete $window.localStorage.user;
      $location.path('/');
    };
  });
