angular.module('MyApp')
  .controller('SignupCtrl', function($scope, $rootScope, $location, $window, $auth, Account) {


      $(document).ready(function() {
        $('#password').keyup(function() {
          $('#result').html(checkStrength($('#password').val()))
        })
        function checkStrength(password) {
          var strength = 0
          if (password.length < 6) {
            $('#result').removeClass()
            $('#result').addClass('short')
            return 'Too short'
          }
          if (password.length > 7) strength += 1
          // If password contains both lower and uppercase characters, increase strength value.
          if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) strength += 1
          // If it has numbers and characters, increase strength value.
          if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/)) strength += 1
          // If it has one special character, increase strength value.
          if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1
          // If it has two special characters, increase strength value.
          if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1
          // Calculated strength value, we can return messages
          // If value is less than 2
          if (strength < 2) {
            $('#result').removeClass()
            $('#result').addClass('weak')
            return 'Weak'
          } else if (strength == 2) {
            $('#result').removeClass()
            $('#result').addClass('good')
            return 'Good'
          } else {
            $('#result').removeClass()
            $('#result').addClass('strong')
            return 'Strong'
          }
        }
      });


      $scope.uid = '';
      $scope.resend = function(){
      Account.resend($scope.uid)
        .then(function(response) {
          $scope.messages = {
            success: [{msg:  "Confirmation Mail re-sent!"}]
          };
        })
        .catch(function(response) {
          $scope.messages = {
            error: Array.isArray(response.data) ? response.data : [response.data]
          };
        });
      return false;

    }
    $scope.signup = function() {
      $auth.signup($scope.user)
        .then(function(response) {
          $scope.messages = {
            success: Array.isArray(response.data) ? response.data : [response.data]
          };
          $scope.uid = response.data.user.id;
          $('html, body').animate({
            scrollTop:0
          }, 500);
          $scope.user = {};
          // $auth.setToken(response);
          // $rootScope.currentUser = response.data.user;
          // $window.localStorage.user = JSON.stringify(response.data.user);
          // $location.path('/');
        })
        .catch(function(response) {
          $scope.messages = {
            error: Array.isArray(response.data) ? response.data : [response.data]
          };

          $('html, body').animate({
            scrollTop:0
          }, 500);
        });
    };

    $scope.authenticate = function(provider) {
      $auth.authenticate(provider)
        .then(function(response) {
          $rootScope.currentUser = response.data.user;
          $window.localStorage.user = JSON.stringify(response.data.user);
          $location.path('/');
        })
        .catch(function(response) {
          if (response.error) {
            $scope.messages = {
              error: [{ msg: response.error }]
            };
          } else if (response.data) {
            $scope.messages = {
              error: [response.data]
            };
          }
        });
    };
  });
